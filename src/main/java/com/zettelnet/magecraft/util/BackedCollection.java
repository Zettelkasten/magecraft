package com.zettelnet.magecraft.util;

import java.util.Collection;
import java.util.Iterator;

public abstract class BackedCollection<T> implements Collection<T> {

	private final Collection<T> base;

	public BackedCollection(Collection<T> set) {
		this.base = set;
	}

	@Override
	public boolean add(T e) {
		return base.add(e);
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		boolean change = false;
		for (T e : c) {
			change |= add(e);
		}
		return change;
	}

	@Override
	public void clear() {
		for (Iterator<T> i = iterator(); i.hasNext();) {
			i.next();
			i.remove();
		}
	}

	@Override
	public boolean contains(Object o) {
		return base.contains(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return base.containsAll(c);
	}

	@Override
	public boolean equals(Object o) {
		return base.equals(o);
	}

	@Override
	public int hashCode() {
		return base.hashCode();
	}

	@Override
	public boolean isEmpty() {
		return base.isEmpty();
	}

	private class BackedSetIterator extends BackedIterator<T> {

		public BackedSetIterator() {
			super(base.iterator());
		}

		@Override
		public void remove() {
			iteratorRemove(base(), current());
		}
	}

	@Override
	public Iterator<T> iterator() {
		return new BackedSetIterator();
	}

	protected abstract void iteratorRemove(Iterator<T> i, T e);

	@Override
	public boolean remove(Object e) {
		return base.remove(e);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		boolean change = false;
		for (Object e : c.toArray()) {
			change |= remove(e);
		}
		return change;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		boolean change = false;
		for (Object e : c) {
			if (!c.contains(e)) {
				change |= remove(e);
			}
		}
		return change;
	}

	@Override
	public int size() {
		return base.size();
	}

	@Override
	public Object[] toArray() {
		return base.toArray();
	}

	@Override
	public <A> A[] toArray(A[] a) {
		return base.toArray(a);
	}

	@Override
	public String toString() {
		return base.toString();
	}

	public Collection<T> base() {
		return base;
	}
}

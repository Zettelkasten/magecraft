package com.zettelnet.magecraft.util;

import java.util.HashSet;
import java.util.Set;

public abstract class BackedSet<T> extends BackedCollection<T> implements Set<T> {

	public BackedSet() {
		this(new HashSet<T>());
	}

	public BackedSet(Set<T> set) {
		super(set);
	}

	@Override
	public Set<T> base() {
		return (Set<T>) super.base();
	}
}

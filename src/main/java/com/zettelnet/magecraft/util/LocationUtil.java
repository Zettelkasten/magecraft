package com.zettelnet.magecraft.util;

import java.util.ArrayList;
import java.util.Collection;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

public class LocationUtil {

	private static final int CHUNK_SIZE = 16;

	public static Collection<Entity> getEntities(final Location l1, Vector size) {
		Collection<Entity> entities = new ArrayList<>();

		Chunk baseChunk = l1.getChunk();
		World world = baseChunk.getWorld();
		Location l2 = l1.clone().add(size);

		for (int chunkX = baseChunk.getX(); chunkX < baseChunk.getX() + size.getX() / CHUNK_SIZE; chunkX++) {
			for (int chunkZ = baseChunk.getZ(); chunkZ < baseChunk.getZ() + size.getZ() / CHUNK_SIZE; chunkZ++) {
				Chunk chunk = world.getChunkAt(chunkX, chunkZ);
				for (Entity entity : chunk.getEntities()) {
					Location loc = entity.getLocation();
					if (l1.getX() <= loc.getX() && loc.getX() <= l2.getX()
							&& l1.getY() <= loc.getY() && loc.getY() <= l2.getY()
							&& l1.getZ() <= loc.getZ() && loc.getZ() <= l2.getZ()) {
						entities.add(entity);
					}
				}
			}
		}
		return entities;
	}
}

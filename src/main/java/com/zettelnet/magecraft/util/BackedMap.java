package com.zettelnet.magecraft.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public abstract class BackedMap<K, V> implements Map<K, V> {

	private final Map<K, V> base;

	private Set<Map.Entry<K, V>> entrySet;
	private Set<K> keySet;
	private Collection<V> values;

	public BackedMap() {
		this(new HashMap<K, V>());
	}

	public BackedMap(Map<K, V> base) {
		this.base = base;
	}

	@Override
	public void clear() {
		for (Iterator<K> i = keySet().iterator(); i.hasNext();) {
			i.next();
			i.remove();
		}
	}

	@Override
	public boolean containsKey(Object key) {
		return base.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return base.containsValue(value);
	}

	private class BackedMapEntrySet extends BackedSet<Map.Entry<K, V>> {

		public BackedMapEntrySet() {
			super(base.entrySet());
		}

		@Override
		protected void iteratorRemove(Iterator<java.util.Map.Entry<K, V>> i, java.util.Map.Entry<K, V> entry) {
			BackedMap.this.iteratorRemoveEntry(i, entry);
		}
	}

	@Override
	public Set<Map.Entry<K, V>> entrySet() {
		return entrySet == null ? entrySet = new BackedMapEntrySet() : entrySet;
	}

	protected abstract void iteratorRemoveEntry(Iterator<Map.Entry<K, V>> i, Map.Entry<K, V> entry);

	@Override
	public boolean equals(Object o) {
		return base.equals(o);
	}

	@Override
	public int hashCode() {
		return base.hashCode();
	}

	@Override
	public V get(Object key) {
		return base.get(key);
	}

	@Override
	public boolean isEmpty() {
		return base.isEmpty();
	}

	private class BackedMapKeySet extends BackedSet<K> {

		public BackedMapKeySet() {
			super(base.keySet());
		}
		
		@Override
		protected void iteratorRemove(Iterator<K> i, K key) {
			BackedMap.this.iteratorRemoveKey(i, key);
		}
	}
	
	protected abstract void iteratorRemoveKey(Iterator<K> i, K key);

	@Override
	public Set<K> keySet() {
		return keySet == null ? keySet = new BackedMapKeySet() : keySet;
	}

	@Override
	public V put(K key, V value) {
		return base.put(key, value);
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
		for (Map.Entry<? extends K, ? extends V> entry : m.entrySet()) {
			put(entry.getKey(), entry.getValue());
		}
	}

	@Override
	public V remove(Object key) {
		return base.remove(key);
	}

	@Override
	public int size() {
		return base.size();
	}
	
	private class BackedMapValues extends BackedCollection<V> {

		public BackedMapValues() {
			super(base.values());
		}

		@Override
		protected void iteratorRemove(Iterator<V> i, V value) {
			iteratorRemoveValue(i, value);
		}
	}
	
	protected abstract void iteratorRemoveValue(Iterator<V> i, V value);

	@Override
	public String toString() {
		return base.toString();
	}
	
	@Override
	public Collection<V> values() {
		return values == null ? values = new BackedMapValues() : values;
	}

	public Map<K, V> base() {
		return base;
	}
}

package com.zettelnet.magecraft.util;

import java.util.Iterator;

public abstract class BackedIterator<T> implements Iterator<T> {

	private final Iterator<T> i;

	private T current;

	public BackedIterator(Iterator<T> iterator) {
		this.i = iterator;
	}

	@Override
	public boolean hasNext() {
		return i.hasNext();
	}

	@Override
	public T next() {
		return current = i.next();
	}

	@Override
	public void remove() {
		i.remove();
	}

	public Iterator<T> base() {
		return i;
	}

	public T current() {
		return current;
	}
}

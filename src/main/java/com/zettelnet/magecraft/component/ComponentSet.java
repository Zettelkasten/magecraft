package com.zettelnet.magecraft.component;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.zettelnet.magecraft.util.BackedSet;

public class ComponentSet extends BackedSet<Component> implements ComponentContainer {

	public ComponentSet() {
		this(new HashSet<Component>());
	}

	public ComponentSet(Set<Component> set) {
		super(set);
	}

	@Override
	public boolean add(Component component) throws NullPointerException {
		if (component == null) {
			throw new NullPointerException("Component cannot be null");
		}
		if (contains(component)) {
			return false;
		}
		super.add(component);
		return true;
	}

	@Override
	protected void iteratorRemove(Iterator<Component> i, Component component) {
		i.remove();
	}

	@Override
	public Set<Component> getComponents() {
		return this;
	}

	@Override
	public boolean addComponents(Collection<Component> components) {
		return addAll(components);
	}

	@Override
	public boolean addComponent(Component component) {
		return add(component);
	}

	@Override
	public boolean removeComponents(Collection<Component> components) {
		return removeAll(components);
	}

	@Override
	public boolean removeComponent(Component component) {
		return remove(component);
	}

	@Override
	public boolean containsComponent(Component component) {
		return contains(component);
	}
}

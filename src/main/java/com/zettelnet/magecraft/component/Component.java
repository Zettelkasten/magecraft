package com.zettelnet.magecraft.component;

import java.util.Collection;

import com.zettelnet.event.EventReceiver;
import com.zettelnet.magecraft.CustomType;

public interface Component extends EventReceiver {

	Collection<CustomType> getRegisteredAt();

	boolean isRegisteredAt(CustomType type);

	boolean setRegisteredAt(CustomType type, boolean registered);

	boolean setEnabled(boolean enabled);

	boolean isEnabled();
}

package com.zettelnet.magecraft.component;

import java.util.Collection;
import java.util.Set;

public interface ComponentContainer {

	Set<Component> getComponents();

	boolean addComponents(Collection<Component> components);

	boolean addComponent(Component component);

	boolean removeComponents(Collection<Component> components);

	boolean removeComponent(Component component);

	boolean containsComponent(Component component);
}

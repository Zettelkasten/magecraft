package com.zettelnet.magecraft.component;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.event.Event;

import com.zettelnet.event.EventManager;
import com.zettelnet.magecraft.CustomType;

public abstract class ComponentBase implements Component {

	private final EventManager<Event> eventManager;
	private final Set<CustomType> registeredAt;

	private boolean enabled;

	public ComponentBase(final EventManager<Event> eventManager) {
		this.eventManager = eventManager;
		this.registeredAt = new HashSet<>(4);
		this.enabled = false;
	}

	public EventManager<Event> getEventManager() {
		return eventManager;
	}

	public Server getServer() {
		return Bukkit.getServer();
	}

	@Override
	public boolean setEnabled(boolean enabled) {
		if (this.enabled == enabled) {
			return false;
		}
		if (enabled) {
			enable();
		} else {
			disable();
		}
		this.enabled = enabled;
		return true;
	}

	protected void enable() {
		eventManager.registerEvents(this);
		onEnable();
	}

	public void onEnable() {
	}

	protected void disable() {
		onDisable();
		eventManager.unregisterEvents(this);
	}

	public void onDisable() {
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public Collection<CustomType> getRegisteredAt() {
		return registeredAt;
	}

	@Override
	public boolean isRegisteredAt(CustomType type) {
		return registeredAt.contains(type);
	}

	@Override
	public boolean setRegisteredAt(CustomType manager, boolean registered) {
		if (registered) {
			return registeredAt.add(manager);
		} else {
			return registeredAt.remove(manager);
		}
	}
}

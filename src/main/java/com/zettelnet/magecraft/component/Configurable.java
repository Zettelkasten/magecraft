package com.zettelnet.magecraft.component;

import com.zettelnet.magecraft.data.DataContainer;

public interface Configurable {

	void configure(DataContainer data);
}

package com.zettelnet.magecraft.event.target;

import com.zettelnet.event.EventTarget;

public class BlockTarget implements EventTarget {

	public static BlockTarget fromString(String target) {
		return new BlockTarget(target.toUpperCase());
	}

	public static final BlockTarget DEFAULT = BlockTarget.fromString("DEFAULT");

	private final boolean unspecific;
	private final String name;

	private BlockTarget(final String name) {
		this.unspecific = false;
		this.name = name;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public boolean isGeneric() {
		return false;
	}

	@Override
	public boolean isAssignableFrom(EventTarget other) {
		if (!(other instanceof BlockTarget)) {
			return false;
		}
		return this.equals(other);
	}

	@Override
	public int hashCode() {
		return unspecific ? 0 : name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BlockTarget other = (BlockTarget) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}

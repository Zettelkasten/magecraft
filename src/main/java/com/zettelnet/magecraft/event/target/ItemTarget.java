package com.zettelnet.magecraft.event.target;

import com.zettelnet.event.EventTarget;

public enum ItemTarget implements EventTarget {

	INVENTORY(null), ARMOR(INVENTORY), HOTBAR(INVENTORY), HAND(HOTBAR);

	private final ItemTarget parent;

	private ItemTarget(ItemTarget parent) {
		this.parent = parent;
	}

	public ItemTarget getParent() {
		return parent;
	}

	@Override
	public boolean isGeneric() {
		return false;
	}

	@Override
	public boolean isAssignableFrom(EventTarget other) {
		if (other == this) {
			return true;
		}
		if (!(other instanceof ItemTarget)) {
			return false;
		}
		ItemTarget parent = ((ItemTarget) other).getParent();
		if (parent == this) {
			return true;
		}
		if (parent == null) {
			return false;
		}
		return isAssignableFrom(parent);
	}
}

package com.zettelnet.magecraft.event.target;

import com.zettelnet.event.EventTarget;

public enum EntityTarget implements EventTarget {

	DEFAULT;

	@Override
	public boolean isGeneric() {
		return false;
	}

	@Override
	public boolean isAssignableFrom(EventTarget other) {
		return this == other;
	}
}

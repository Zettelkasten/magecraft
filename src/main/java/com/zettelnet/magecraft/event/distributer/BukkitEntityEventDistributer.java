package com.zettelnet.magecraft.event.distributer;

import org.bukkit.entity.Entity;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityEvent;

import com.zettelnet.event.DistributionExecutor;
import com.zettelnet.event.EventDistributer;
import com.zettelnet.magecraft.CustomEntity;
import com.zettelnet.magecraft.EntityManager;
import com.zettelnet.magecraft.event.target.EntityTarget;

public class BukkitEntityEventDistributer implements EventDistributer<Event> {

	private final EntityManager manager;

	public BukkitEntityEventDistributer(final EntityManager manager) {
		this.manager = manager;
	}

	@Override
	public void handleEvent(DistributionExecutor<Event> executor, Event event) {
		if (event instanceof EntityEvent) {
			handleEntity(executor, event, ((EntityEvent) event).getEntity(), EntityTarget.DEFAULT);
		}
	}

	protected void handleEntity(DistributionExecutor<Event> executor, Event event, Entity handle, EntityTarget requiredTarget) {
		CustomEntity entity = manager.getEntityByHandle(handle);
		if (entity == null) {
			return;
		}
		executor.executeEvent(event, entity, requiredTarget);
	}
}

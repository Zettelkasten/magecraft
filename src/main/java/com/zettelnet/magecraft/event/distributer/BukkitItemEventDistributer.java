package com.zettelnet.magecraft.event.distributer;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityEvent;
import org.bukkit.event.inventory.InventoryEvent;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import com.zettelnet.event.DistributionExecutor;
import com.zettelnet.event.EventDistributer;
import com.zettelnet.magecraft.CustomItem;
import com.zettelnet.magecraft.ItemManager;
import com.zettelnet.magecraft.event.target.ItemTarget;

public class BukkitItemEventDistributer implements EventDistributer<Event> {

	private final ItemManager manager;

	public BukkitItemEventDistributer(final ItemManager manager) {
		this.manager = manager;
	}

	@Override
	public void handleEvent(DistributionExecutor<Event> executor, Event event) {
		if (event instanceof PlayerEvent) {
			handlePlayerItems(executor, event, ((PlayerEvent) event).getPlayer());
		}
		if (event instanceof EntityEvent && ((EntityEvent) event).getEntity() instanceof Player) {
			handlePlayerItems(executor, event, (Player) ((EntityEvent) event).getEntity());
		}
		if (event instanceof InventoryEvent) {
			InventoryEvent e = (InventoryEvent) event;
			Inventory inv = e.getInventory();
			for (ItemStack handle : inv.getContents()) {
				handleItem(executor, event, handle, inv, ItemTarget.INVENTORY);
			}
		}
		if (event instanceof BlockBreakEvent) {
			handlePlayerItems(executor, event, ((BlockBreakEvent) event).getPlayer());
		}
		if (event instanceof BlockPlaceEvent) {
			handlePlayerItems(executor, event, ((BlockPlaceEvent) event).getPlayer());
		}
		if (event instanceof InventoryEvent) {
			InventoryEvent e = (InventoryEvent) event;
			if (e.getInventory().getHolder() instanceof Player) {
				handlePlayerItems(executor, event, (Player) e.getInventory().getHolder());
			}
		}
	}

	protected void handlePlayerItems(DistributionExecutor<Event> executor, Event event, Player player) {
		PlayerInventory inv = player.getInventory();
		ItemStack[] contents = inv.getContents();
		for (int i = 0; i < contents.length; i++) {
			ItemTarget target = ItemTarget.INVENTORY;
			if (i < 9) {
				target = ItemTarget.HOTBAR;
				if (i == inv.getHeldItemSlot()) {
					target = ItemTarget.HAND;
				}
			}
			handleItem(executor, event, contents[i], inv, target);
		}
		for (ItemStack handle : inv.getArmorContents()) {
			handleItem(executor, event, handle, inv, ItemTarget.ARMOR);
		}
	}

	protected void handleItem(DistributionExecutor<Event> executor, Event event, ItemStack handle, Inventory inventory, ItemTarget requiredTarget) {
		CustomItem item = manager.getItemByHandle(handle, inventory);
		if (item == null) {
			return;
		}
		executor.executeEvent(event, item, requiredTarget);
	}
}

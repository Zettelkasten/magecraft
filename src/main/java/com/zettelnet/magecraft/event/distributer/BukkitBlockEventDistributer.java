package com.zettelnet.magecraft.event.distributer;

import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.event.Event;
import org.bukkit.event.block.BlockEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.enchantment.PrepareItemEnchantEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityCombustByBlockEvent;
import org.bukkit.event.entity.EntityCreatePortalEvent;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBedLeaveEvent;
import org.bukkit.event.player.PlayerBucketEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.vehicle.VehicleBlockCollisionEvent;
import org.bukkit.event.world.PortalCreateEvent;
import org.bukkit.event.world.StructureGrowEvent;

import com.zettelnet.event.DistributionExecutor;
import com.zettelnet.event.EventDistributer;
import com.zettelnet.magecraft.BlockManager;
import com.zettelnet.magecraft.CustomBlock;
import com.zettelnet.magecraft.event.target.BlockTarget;

public class BukkitBlockEventDistributer implements EventDistributer<Event> {

	private final BlockManager manager;

	public BukkitBlockEventDistributer(final BlockManager manager) {
		this.manager = manager;
	}

	@Override
	public void handleEvent(DistributionExecutor<Event> executor, Event event) {
		// org.bukkit.event.block
		if (event instanceof BlockEvent) {
			handleBlock(executor, event, ((BlockEvent) event).getBlock());

			if (event instanceof BlockFromToEvent) {
				handleBlock(executor, event, ((BlockFromToEvent) event).getToBlock(), "to");
			}
			if (event instanceof BlockIgniteEvent) {
				handleBlock(executor, event, ((BlockIgniteEvent) event).getIgnitingBlock(), "igniting");
			}
			if (event instanceof BlockPistonExtendEvent) {
				BlockPistonExtendEvent e = (BlockPistonExtendEvent) event;
				for (Block block : e.getBlocks()) {
					handleBlock(executor, event, block, "moved");
				}
			}
			if (event instanceof BlockPlaceEvent) {
				handleBlock(executor, event, ((BlockPlaceEvent) event).getBlockAgainst(), "against");
			}
			if (event instanceof BlockSpreadEvent) {
				handleBlock(executor, event, ((BlockSpreadEvent) event).getSource(), "source");
			}
		}
		// org.bukkit.event.enchantment
		if (event instanceof EnchantItemEvent) {
			handleBlock(executor, event, ((EnchantItemEvent) event).getEnchantBlock());
		}
		if (event instanceof PrepareItemEnchantEvent) {
			handleBlock(executor, event, ((PrepareItemEnchantEvent) event).getEnchantBlock());
		}
		// org.bukkit.event.entity
		if (event instanceof EntityChangeBlockEvent) {
			handleBlock(executor, event, ((EntityChangeBlockEvent) event).getBlock());
		}
		if (event instanceof EntityCombustByBlockEvent) {
			handleBlock(executor, event, ((EntityCombustByBlockEvent) event).getCombuster());
		}
		if (event instanceof EntityCreatePortalEvent) {
			EntityCreatePortalEvent e = (EntityCreatePortalEvent) event;
			for (BlockState state : e.getBlocks()) {
				handleBlock(executor, event, state.getBlock());
			}
		}
		if (event instanceof EntityDamageByBlockEvent) {
			handleBlock(executor, event, ((EntityDamageByBlockEvent) event).getDamager());
		}
		if (event instanceof EntityExplodeEvent) {
			EntityExplodeEvent e = (EntityExplodeEvent) event;
			for (Block block : e.blockList()) {
				handleBlock(executor, event, block);
			}
		}
		if (event instanceof EntityInteractEvent) {
			handleBlock(executor, event, ((EntityInteractEvent) event).getBlock());
		}
		// org.bukkit.event.hanging
		if (event instanceof HangingPlaceEvent) {
			handleBlock(executor, event, ((HangingPlaceEvent) event).getBlock());
		}
		// org.bukkit.event.inventory [none]
		// org.bukkit.event.painting [none]
		// org.bukkit.event.player
		if (event instanceof PlayerBedEnterEvent) {
			handleBlock(executor, event, ((PlayerBedEnterEvent) event).getBed());
		}
		if (event instanceof PlayerBedLeaveEvent) {
			handleBlock(executor, event, ((PlayerBedLeaveEvent) event).getBed());
		}
		if (event instanceof PlayerBucketEvent) {
			handleBlock(executor, event, ((PlayerBucketEvent) event).getBlockClicked());
		}
		if (event instanceof PlayerInteractEvent) {
			handleBlock(executor, event, ((PlayerInteractEvent) event).getClickedBlock());
		}
		// org.bukkit.event.server [none]
		// org.bukkit.event.vehicle
		if (event instanceof VehicleBlockCollisionEvent) {
			handleBlock(executor, event, ((VehicleBlockCollisionEvent) event).getBlock());
		}
		// org.bukkit.event.weather [none]
		// org.bukkit.event.world
		if (event instanceof PortalCreateEvent) {
			PortalCreateEvent e = (PortalCreateEvent) event;
			for (Block block : e.getBlocks()) {
				handleBlock(executor, event, block);
			}
		}
		if (event instanceof StructureGrowEvent) {
			StructureGrowEvent e = (StructureGrowEvent) event;
			for (BlockState state : e.getBlocks()) {
				handleBlock(executor, event, state.getBlock());
			}
		}
	}

	protected void handleBlock(DistributionExecutor<Event> executor, Event event, Block handle) {
		handleBlock(executor, event, handle, BlockTarget.DEFAULT);
	}

	protected void handleBlock(DistributionExecutor<Event> executor, Event event, Block handle, String requiredTarget) {
		handleBlock(executor, event, handle, BlockTarget.fromString(requiredTarget));
	}

	protected void handleBlock(DistributionExecutor<Event> executor, Event event, Block handle, BlockTarget requiredTarget) {
		CustomBlock block = manager.getBlockByHandle(handle);
		if (block == null) {
			return;
		}
		executor.executeEvent(event, block, requiredTarget);
	}
}

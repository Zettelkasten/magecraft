package com.zettelnet.magecraft.event.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.bukkit.event.EventPriority;

import com.zettelnet.magecraft.event.target.EntityTarget;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EntityHandler {

	EventPriority priority() default EventPriority.NORMAL;

	boolean ignoreCancelled() default false;

	EntityTarget target() default EntityTarget.DEFAULT;
}

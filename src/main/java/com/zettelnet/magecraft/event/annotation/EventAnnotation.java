package com.zettelnet.magecraft.event.annotation;

import org.bukkit.event.EventHandler;

import com.zettelnet.event.EventPriority;
import com.zettelnet.event.EventTarget;
import com.zettelnet.magecraft.event.target.BlockTarget;

/**
 * Wraps the fields of all event annotations to pass them to another method.
 * 
 * @author Zettelkasten
 *
 */
public class EventAnnotation {

	private final EventPriority priority;
	private final boolean ignoreCancelled;
	private final EventTarget target;

	/**
	 * Constructs a new EventAnnotation with the field values.
	 * 
	 * @param priority
	 *            the priority of the event
	 * @param ignoreCancelled
	 *            whether the event should be ignored if cancelled
	 * @param target
	 *            the event target specifing the CustomObject
	 */
	public EventAnnotation(final EventPriority priority, final boolean ignoreCancelled, final EventTarget target) {
		this.priority = priority;
		this.ignoreCancelled = ignoreCancelled;
		this.target = target;
	}

	/**
	 * Return the priority of the event. Lower priority events will be executed
	 * before high priority events.
	 * 
	 * @return the priority
	 */
	public EventPriority priority() {
		return priority;
	}

	/**
	 * Returns whether the event should be ignored if cancelled
	 * 
	 * @return whether the event should be ignored if cancelled
	 */
	public boolean ignoreCancelled() {
		return ignoreCancelled;
	}

	/**
	 * Returns the event target specifing the CustomObject.
	 * 
	 * @return the event target
	 */
	public EventTarget target() {
		return target;
	}

	/**
	 * Utility method for constructing an EventAnnotation object from an
	 * {@link EventHandler}. The constructed event annotation will have the
	 * event target {@link EventTarget#GENERIC}.
	 * 
	 * @param annotation
	 *            the annotation to get the fields from
	 * @return a new EventAnnotation
	 */
	public static EventAnnotation fromEventHandler(EventHandler annotation) {
		return new EventAnnotation(EventPriority.fromSlot(annotation.priority().ordinal()), annotation.ignoreCancelled(), EventTarget.GENERIC);
	}

	/**
	 * Utility method for constructing an EventAnnotation object from an
	 * {@link ItemHandler}.
	 * 
	 * @param annotation
	 *            the annotation to get the fields from
	 * @return a new EventAnnotation
	 */
	public static EventAnnotation fromItemHandler(ItemHandler annotation) {
		return new EventAnnotation(EventPriority.fromSlot(annotation.priority().ordinal()), annotation.ignoreCancelled(), annotation.target());
	}

	/**
	 * Utility method for constructing an EventAnnotation object from an
	 * {@link BlockHandler}.
	 * 
	 * @param annotation
	 *            the annotation to get the fields from
	 * @return a new EventAnnotation
	 */
	public static EventAnnotation fromBlockHandler(BlockHandler annotation) {
		return new EventAnnotation(EventPriority.fromSlot(annotation.priority().ordinal()), annotation.ignoreCancelled(), BlockTarget.fromString(annotation.target()));
	}

	/**
	 * Utility method for constructing an EventAnnotation object from an
	 * {@link BlockHandler}.
	 * 
	 * @param annotation
	 *            the annotation to get the fields from
	 * @return a new EventAnnotation
	 */
	public static EventAnnotation fromEntityHandler(EntityHandler annotation) {
		return new EventAnnotation(EventPriority.fromSlot(annotation.priority().ordinal()), annotation.ignoreCancelled(), annotation.target());
	}
}

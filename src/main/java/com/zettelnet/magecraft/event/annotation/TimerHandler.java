package com.zettelnet.magecraft.event.annotation;

public @interface TimerHandler {

	long period() default 60L;

	long delay() default 0L;
}

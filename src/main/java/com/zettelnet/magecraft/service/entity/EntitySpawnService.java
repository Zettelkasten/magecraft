package com.zettelnet.magecraft.service.entity;

import org.bukkit.Location;
import org.bukkit.entity.Entity;

import com.zettelnet.magecraft.CustomType;
import com.zettelnet.magecraft.service.Service;

public interface EntitySpawnService extends Service {

	Entity spawnEntity(CustomType type, Location location);
}

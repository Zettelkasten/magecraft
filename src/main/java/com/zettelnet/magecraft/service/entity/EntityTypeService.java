package com.zettelnet.magecraft.service.entity;

import org.bukkit.entity.EntityType;

import com.zettelnet.magecraft.service.Service;

public interface EntityTypeService extends Service {

	EntityType getEntityType();
}

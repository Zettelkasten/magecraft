package com.zettelnet.magecraft.service.item;

import org.bukkit.Material;

import com.zettelnet.magecraft.service.Service;

public interface ItemTypeService extends Service {

	Material getMaterial();

	short getDamage();
}

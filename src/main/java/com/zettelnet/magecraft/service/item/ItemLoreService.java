package com.zettelnet.magecraft.service.item;

import java.util.List;

import com.zettelnet.magecraft.CustomItem;
import com.zettelnet.magecraft.service.Service;

public interface ItemLoreService extends Service {

	List<String> getLore(CustomItem item);

	boolean hasLore(CustomItem item);

	void setLore(CustomItem item, List<String> lore);

	void setLore(CustomItem item, String... lore);
}

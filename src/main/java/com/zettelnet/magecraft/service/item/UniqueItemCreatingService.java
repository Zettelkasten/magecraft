package com.zettelnet.magecraft.service.item;

import java.util.UUID;

import org.bukkit.inventory.ItemStack;

import com.zettelnet.magecraft.CustomType;
import com.zettelnet.magecraft.service.Service;

public interface UniqueItemCreatingService extends Service {

	ItemStack createItem(CustomType type, UUID uniqueId);
}

package com.zettelnet.magecraft.service.item;

import org.bukkit.inventory.ItemStack;

import com.zettelnet.magecraft.CustomType;
import com.zettelnet.magecraft.service.Service;

public interface ItemCreatingService extends Service {

	ItemStack createItem(CustomType type, int amount);
}

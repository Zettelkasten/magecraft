package com.zettelnet.magecraft.service;

import java.util.Collection;
import java.util.Map;

/**
 * A map-like object that contains {@link Service}s for corresponding tasks. A
 * task is the class of an interface that specifies certain methods a service
 * must implement. A service container may contain a service that performs
 * multiple tasks.
 * 
 * @author Zettelkasten
 * @see Service
 * @see Map
 * @see ServiceMap default implementation
 */
public interface ServiceContainer {

	/**
	 * Returns a {@link Collection} view of the {@link Service}s contained in
	 * this container. The collection is backed by this container, so changes
	 * are reflected in the collection and vice-versa. The collection only
	 * supports element removal.
	 * 
	 * @return a collection view of the {@link Service}s contained in this
	 *         container
	 */
	Collection<Service> getServices();

	/**
	 * Returns a {@link Map} view of the {@link Service}s contained in this
	 * container by the task they are performing. The map is backed by this
	 * container, so changes are reflected in the collection and vice-versa. The
	 * map supports all operations. The {@link Map#get(Object)} operation will
	 * not throw a {@link ServiceUnavailableException} but return
	 * <code>null</code> instead if no service is available.
	 * 
	 * @return a map view of the {@link Service}s contained in this container
	 */
	Map<Class<? extends Service>, Service> getServicesTasks();

	/**
	 * Returns the {@link Service} that performs the specific task, or throws a
	 * {@link ServiceUnavailableException} if this container contains no service
	 * that performs the specified task.
	 * 
	 * @param task
	 *            the task whose performing service should be returned
	 * @return the service performing the task
	 * @throws ServiceUnavailableException
	 *             if this container does not contain a service performing that
	 *             task
	 */
	<T extends Service> T getService(Class<T> task) throws ServiceUnavailableException;

	Service addService(Service service, Class<? extends Service> task) throws IllegalArgumentException;

	void addService(Service service, Collection<Class<? extends Service>> tasks) throws IllegalArgumentException;

	void addServices(Map<Class<? extends Service>, Service> services) throws IllegalArgumentException;

	void removeService(Service service);

	void removeServices(Collection<Service> services);

	Service removeServiceTask(Class<? extends Service> task);

	void removeServiceTasks(Collection<Class<? extends Service>> tasks);

	boolean containsService(Service service);

	boolean containsServiceTask(Class<? extends Service> task);

	ServiceContainer getBackingServiceContainer();

	ServiceContainer setBackingServiceContainer(ServiceContainer container);

	boolean hasBackingServiceContainer();
}

package com.zettelnet.magecraft.service;

import com.zettelnet.magecraft.CustomType;

/**
 * An exception thrown when a {@link Service} is requested but the
 * {@link CustomType} does not contain it.
 * 
 * @author Zettelkasten
 *
 */
public class ServiceUnavailableException extends RuntimeException {
	private static final long serialVersionUID = 1L;

}

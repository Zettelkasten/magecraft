package com.zettelnet.magecraft.service.block;

import org.bukkit.entity.FallingBlock;

import com.zettelnet.magecraft.CustomBlock;
import com.zettelnet.magecraft.service.Service;

public interface BlockTransformToFallingBlockService extends Service {

	FallingBlock transformToFallingBlock(CustomBlock block);
}

package com.zettelnet.magecraft.service.block;

import org.bukkit.material.MaterialData;

import com.zettelnet.magecraft.service.Service;

public interface BlockTypeService extends Service {

	MaterialData getMaterial();

	MaterialData getPlacingMaterial();
}

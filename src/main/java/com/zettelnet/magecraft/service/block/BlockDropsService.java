package com.zettelnet.magecraft.service.block;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.zettelnet.magecraft.CustomBlock;
import com.zettelnet.magecraft.CustomItem;
import com.zettelnet.magecraft.service.Service;

/**
 * Block service used to determine the drops of the block when broken. It is up
 * to the service to decide what tool has to be used to break a block that it
 * will drop items.
 * 
 * @author Zettelkasten
 *
 */
public interface BlockDropsService extends Service {

	/**
	 * An unmodifiable set containing all pickaxe materials.
	 */
	public static final Set<Material> TOOL_PICKAXE = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(Material.WOOD_PICKAXE, Material.STONE_PICKAXE, Material.GOLD_PICKAXE, Material.IRON_PICKAXE, Material.DIAMOND_PICKAXE)));

	/**
	 * An unmodifiable set containing all axe materials.
	 */
	public static final Set<Material> TOOL_AXE = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(Material.WOOD_AXE, Material.STONE_AXE, Material.GOLD_AXE, Material.IRON_AXE, Material.DIAMOND_AXE)));

	/**
	 * An unmodifiable set containing all spade (shovel) materials.
	 */
	public static final Set<Material> TOOL_SPADE = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(Material.WOOD_SPADE, Material.STONE_SPADE, Material.GOLD_SPADE, Material.IRON_SPADE, Material.DIAMOND_SPADE)));

	/**
	 * Returns a collection containing all items that would be dropped if this
	 * block is broken.
	 * 
	 * @param block
	 *            the block to check
	 * @return a collection of all dropped items
	 */
	Collection<ItemStack> getDrops(CustomBlock block);

	/**
	 * Returns a collection containing all items that would be dropped if the
	 * block was broken with a specified tool. If the block cannot be broken
	 * with that tool, an empty collection will be returned.
	 * 
	 * @param block
	 *            the block to check
	 * @param tool
	 *            the tool used to break the block or <tt>null</tt> if no tool
	 *            is used
	 * @return a collection of all dropped items
	 */
	Collection<ItemStack> getDrops(CustomBlock block, ItemStack tool);

	/**
	 * Returns a collection containing all items that would be dropped if the
	 * block was broken with a specified tool. If the block cannot be broken
	 * with that tool, an empty collection will be returned.
	 * 
	 * @param block
	 *            the block to check
	 * @param tool
	 *            the tool used to break the block or <tt>null</tt> if no tool
	 *            is used
	 * @return a collection of all dropped items
	 */
	Collection<ItemStack> getDrops(CustomBlock block, CustomItem tool);
}

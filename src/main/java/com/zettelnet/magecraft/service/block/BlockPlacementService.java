package com.zettelnet.magecraft.service.block;

import org.bukkit.inventory.ItemStack;

import com.zettelnet.magecraft.CustomBlock;
import com.zettelnet.magecraft.CustomItem;
import com.zettelnet.magecraft.service.Service;

/**
 * Block service used to place and break blocks.
 * 
 * @author Zettelkasten
 * @service.dependency {@link BlockTypeService}
 * @service.dependency {@link BlockDropsService}
 *
 */
public interface BlockPlacementService extends Service {

	/**
	 * Places a block if the location is not occupied.
	 * 
	 * @param block
	 *            the block to place
	 * @return whether the block has been set
	 */
	boolean placeBlock(CustomBlock block);

	/**
	 * Sets a block and overrides any previous state of the block.
	 * 
	 * @param block
	 *            the block to set
	 * @return wheather the block has been set
	 */
	boolean setBlock(CustomBlock block);

	/**
	 * Breaks a block and drops the items.
	 * 
	 * @param block
	 *            the block to break
	 * @return whether the block has been removed
	 */
	boolean breakBlock(CustomBlock block);

	/**
	 * Breaks a block with a specified tool and drops the items as if a player
	 * has digged it.
	 * 
	 * @param block
	 *            the block to break
	 * @param tool
	 *            the tool used to break the block or <tt>null</tt> if no tool
	 *            is used
	 * @return whether the block has been removed
	 */
	boolean breakBlock(CustomBlock block, ItemStack tool);

	/**
	 * Breaks a block with a specified tool and drops the items as if a player
	 * has digged it.
	 * 
	 * @param block
	 *            the block to break
	 * @param tool
	 *            the tool used to break the block or <tt>null</tt> if no tool
	 *            is used
	 * @return whether the block has been removed
	 */
	boolean breakBlock(CustomBlock block, CustomItem tool);

	/**
	 * Breaks a block without dropping any items.
	 * 
	 * @param block
	 *            the block to remove
	 * @return whether the block has been removed
	 */
	boolean removeBlock(CustomBlock block);
}

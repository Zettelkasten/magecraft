package com.zettelnet.magecraft.service.block;

import org.bukkit.block.BlockFace;

import com.zettelnet.magecraft.CustomBlock;
import com.zettelnet.magecraft.service.Service;

public interface BlockFacingService extends Service {

	BlockFace getFacing(CustomBlock block);

	BlockFace setFacing(CustomBlock block, BlockFace facing);
}

package com.zettelnet.magecraft.service;

import com.zettelnet.magecraft.component.Component;

/**
 * 
 * Represents a service that offeres a set of methods for other services and
 * {@link Component}s to use.
 * 
 * @author Zettelkasten
 *
 */
public interface Service {

}

package com.zettelnet.magecraft.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.zettelnet.magecraft.util.BackedMap;

public class ServiceMap extends BackedMap<Class<? extends Service>, Service> implements ServiceContainer {

	private ServiceContainer backingContainer;

	public ServiceMap() {
		this(null);
	}

	public ServiceMap(ServiceContainer backingContainer) {
		this(backingContainer, new HashMap<Class<? extends Service>, Service>());
	}

	public ServiceMap(ServiceContainer backingContainer, Map<Class<? extends Service>, Service> base) {
		super(base);
		this.backingContainer = backingContainer;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Service get(Object task) {
		if (!containsKey(task)) {
			if (!hasBackingServiceContainer()) {
				return null;
			}
			try {
				Class<? extends Service> clazz = (Class<? extends Service>) task;
				if (backingContainer.containsServiceTask(clazz)) {
					return backingContainer.getService(clazz);
				} else {
					return null;
				}
			} catch (ClassCastException e) {
				return null;
			}
		}
		return super.get(task);
	}

	@Override
	public Service put(Class<? extends Service> task, Service service) throws IllegalArgumentException {
		if (!task.isAssignableFrom(service.getClass())) {
			throw new IllegalArgumentException("Service does not implement task class");
		}
		return super.put(task, service);
	}

	@Override
	public void putAll(Map<? extends Class<? extends Service>, ? extends Service> map) throws IllegalArgumentException {
		for (Map.Entry<? extends Class<? extends Service>, ? extends Service> entry : map.entrySet()) {
			put(entry.getKey(), entry.getValue());
		}
	}

	@Override
	public Collection<Service> getServices() {
		return values();
	}

	@Override
	public Map<Class<? extends Service>, Service> getServicesTasks() {
		return this;
	}

	@Override
	public <T extends Service> T getService(Class<T> task) throws ServiceUnavailableException {
		if (!containsServiceTask(task)) {
			if (hasBackingServiceContainer()) {
				return backingContainer.getService(task);
			} else {
				throw new ServiceUnavailableException();
			}
		}
		return task.cast(get(task));
	}

	@Override
	public Service addService(Service service, Class<? extends Service> task) throws IllegalArgumentException {
		return put(task, service);
	}

	@Override
	public void addService(Service service, Collection<Class<? extends Service>> tasks) throws IllegalArgumentException {
		for (Class<? extends Service> task : tasks) {
			addService(service, task);
		}
	}

	@Override
	public void addServices(Map<Class<? extends Service>, Service> services) throws IllegalArgumentException {
		putAll(services);
	}

	@Override
	public void removeService(Service service) {
		while (getServices().remove(service));
	}

	@Override
	public void removeServices(Collection<Service> services) {
		for (Service service : services) {
			removeService(service);
		}
	}

	@Override
	public Service removeServiceTask(Class<? extends Service> task) {
		return remove(task);
	}

	@Override
	public void removeServiceTasks(Collection<Class<? extends Service>> tasks) {
		for (Class<? extends Service> task : tasks) {
			removeServiceTask(task);
		}
	}

	@Override
	public boolean containsService(Service service) {
		return containsValue(service);
	}

	@Override
	public boolean containsServiceTask(Class<? extends Service> task) {
		return containsKey(task);
	}

	@Override
	public ServiceContainer getBackingServiceContainer() {
		return backingContainer;
	}

	@Override
	public ServiceContainer setBackingServiceContainer(ServiceContainer container) {
		ServiceContainer old = backingContainer;
		this.backingContainer = container;
		return old;
	}

	@Override
	public boolean hasBackingServiceContainer() {
		return backingContainer != null;
	}

	@Override
	protected void iteratorRemoveEntry(Iterator<Map.Entry<Class<? extends Service>, Service>> i, java.util.Map.Entry<Class<? extends Service>, Service> entry) {
		i.remove();
	}

	@Override
	protected void iteratorRemoveKey(Iterator<Class<? extends Service>> i, Class<? extends Service> key) {
		i.remove();
	}

	@Override
	protected void iteratorRemoveValue(Iterator<Service> i, Service value) {
		i.remove();
	}
}

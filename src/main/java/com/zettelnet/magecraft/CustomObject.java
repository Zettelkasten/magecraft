package com.zettelnet.magecraft;

import org.bukkit.Server;
import org.bukkit.plugin.Plugin;

import com.zettelnet.magecraft.data.DataContainer;

/**
 * 
 * Represents an instance of a {@link CustomType}. This object will be passed to
 * components, services and other handlers and only represents one specific
 * instance.
 * <p>
 * Multiple custom objects of the same type may store data indepentent from each
 * other using {@link #getData()}.
 *
 */
public interface CustomObject {

	CustomType getType();

	Plugin getPlugin();

	Server getServer();

	DataContainer getData();

	DataContainer getTransientData();
}

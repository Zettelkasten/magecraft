package com.zettelnet.magecraft;

import java.util.ArrayList;
import java.util.Collections;

import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.zettelnet.event.EventManager;
import com.zettelnet.event.GenericEventDistributer;
import com.zettelnet.magecraft.event.distributer.BukkitBlockEventDistributer;
import com.zettelnet.magecraft.event.distributer.BukkitEntityEventDistributer;
import com.zettelnet.magecraft.event.distributer.BukkitItemEventDistributer;
import com.zettelnet.magecraft.magic.MagicBlockManager;
import com.zettelnet.magecraft.magic.MagicEntityManager;
import com.zettelnet.magecraft.magic.MagicTraceableItemManager;
import com.zettelnet.magecraft.magic.MagicTypeManager;
import com.zettelnet.magecraft.magic.event.MagicEventManager;
import com.zettelnet.magecraft.magic.service.block.MagicBlockDropsService;
import com.zettelnet.magecraft.magic.service.block.MagicBlockPlacementService;
import com.zettelnet.magecraft.magic.service.block.MagicBlockTransformToFallingBlockService;
import com.zettelnet.magecraft.magic.service.item.MagicItemLoreService;
import com.zettelnet.magecraft.service.ServiceContainer;
import com.zettelnet.magecraft.service.ServiceMap;
import com.zettelnet.magecraft.service.block.BlockDropsService;
import com.zettelnet.magecraft.service.block.BlockPlacementService;
import com.zettelnet.magecraft.service.block.BlockTransformToFallingBlockService;
import com.zettelnet.magecraft.service.item.ItemLoreService;

public final class Magecraft extends JavaPlugin {

	public static Magecraft getInstace() {
		return getPlugin(Magecraft.class);
	}

	private final TypeManager typeManager;

	private final TraceableItemManager itemManager;
	private final BlockManager blockManager;
	private final EntityManager entityManager;

	private final EventManager<Event> eventManager;

	private final ServiceContainer serviceDefaults;

	public Magecraft() {
		this.typeManager = new MagicTypeManager();

		this.itemManager = new MagicTraceableItemManager(typeManager);
		this.blockManager = new MagicBlockManager(typeManager);
		this.entityManager = new MagicEntityManager(typeManager);

		this.eventManager = new MagicEventManager(this);
		this.eventManager.addDistributer(new GenericEventDistributer<Event>());
		this.eventManager.addDistributer(new BukkitItemEventDistributer(itemManager));
		this.eventManager.addDistributer(new BukkitBlockEventDistributer(blockManager));
		this.eventManager.addDistributer(new BukkitEntityEventDistributer(entityManager));

		this.serviceDefaults = new ServiceMap();
		this.serviceDefaults.addService(new MagicBlockDropsService(Collections.<ItemStack> emptyList()), BlockDropsService.class);
		this.serviceDefaults.addService(new MagicBlockPlacementService(), BlockPlacementService.class);
		this.serviceDefaults.addService(new MagicBlockTransformToFallingBlockService(), BlockTransformToFallingBlockService.class);
		this.serviceDefaults.addService(new MagicItemLoreService(), ItemLoreService.class);
	}

	@Override
	public void onDisable() {
		itemManager.unregisterAllItems();
		blockManager.unregisterAllBlocks();
		entityManager.unregisterAllEntities();

		// creating new list to avoid ConcurrentModificationException
		for (CustomType type : new ArrayList<>(typeManager.getRegisteredTypes())) {
			type.setEnabled(false);
			typeManager.unregisterType(type);
		}
	}

	public TypeManager getTypeManager() {
		return typeManager;
	}

	public TraceableItemManager getItemManager() {
		return itemManager;
	}

	public BlockManager getBlockManager() {
		return blockManager;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public EventManager<Event> getEventManager() {
		return eventManager;
	}

	public ServiceContainer getServiceDefaults() {
		return serviceDefaults;
	}
}

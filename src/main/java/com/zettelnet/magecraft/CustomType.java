package com.zettelnet.magecraft;

import java.util.Collection;

import org.bukkit.Server;
import org.bukkit.plugin.Plugin;

import com.zettelnet.magecraft.component.ComponentContainer;
import com.zettelnet.magecraft.service.ServiceContainer;

public interface CustomType extends ServiceContainer, ComponentContainer {

	Plugin getPlugin();

	Server getServer();

	String getName();

	boolean isEnabled();

	boolean setEnabled(boolean enabled);

	Collection<TypeManager> getRegisteredAt();

	boolean isRegisteredAt(TypeManager manager);

	boolean setRegisteredAt(TypeManager manager, boolean registered);
}

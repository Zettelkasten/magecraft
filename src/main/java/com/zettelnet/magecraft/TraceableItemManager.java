package com.zettelnet.magecraft;

import java.util.Collection;
import java.util.UUID;

import org.bukkit.inventory.Inventory;

public interface TraceableItemManager extends ItemManager {

	Collection<TraceableItem> getRegisteredItems();

	Collection<TraceableItem> getRegisteredItems(CustomType type);

	TraceableItem getItemByUniqueId(UUID uniqueId);

	void registerItems(Collection<TraceableItem> items);

	boolean registerItem(TraceableItem item);

	void unregisterAllItems();

	void unregisterItems(Collection<TraceableItem> items);

	boolean unregisterItem(TraceableItem item);

	boolean isItemRegistered(TraceableItem item);

	boolean isItemUniqueIdRegistered(UUID uniqueId);
	
	@Deprecated
	TraceableItem createTraceableItem(CustomType type, Inventory inventory);
}

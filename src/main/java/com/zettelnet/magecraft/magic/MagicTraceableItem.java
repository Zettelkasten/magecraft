package com.zettelnet.magecraft.magic;

import java.util.Collection;
import java.util.HashSet;
import java.util.UUID;

import org.bukkit.Server;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import com.zettelnet.magecraft.CustomType;
import com.zettelnet.magecraft.TraceableItem;
import com.zettelnet.magecraft.TraceableItemManager;
import com.zettelnet.magecraft.data.DataContainer;
import com.zettelnet.magecraft.data.DataMap;

public class MagicTraceableItem implements TraceableItem {

	private final CustomType type;
	private final UUID uid;

	private ItemStack handle;
	private Inventory inventory;

	private final Collection<TraceableItemManager> registeredAt;

	private final DataContainer data;
	private final DataContainer transientData;

	public MagicTraceableItem(final CustomType type, final UUID uid, ItemStack handle, Inventory inventory) {
		this.type = type;
		this.uid = uid;

		this.handle = handle;
		this.inventory = inventory;

		this.registeredAt = new HashSet<>(1);

		this.data = new DataMap();
		this.transientData = new DataMap();
	}

	@Override
	public CustomType getType() {
		return type;
	}

	@Override
	public Plugin getPlugin() {
		return type.getPlugin();
	}

	@Override
	public Server getServer() {
		return type.getServer();
	}

	@Override
	public DataContainer getData() {
		return data;
	}

	@Override
	public DataContainer getTransientData() {
		return transientData;
	}

	@Override
	public UUID getUniqueId() {
		return uid;
	}

	@Override
	public ItemStack getHandle() {
		return handle;
	}

	@Override
	public boolean updateHandle(ItemStack handle) {
		if (this.handle == handle) {
			return false;
		}
		this.handle = handle;
		return true;
	}

	@Override
	public Inventory getInventory() {
		return inventory;
	}

	@Override
	public boolean updateInventory(Inventory inventory) {
		if (this.inventory == inventory) {
			return false;
		}
		this.inventory = inventory;
		return true;
	}

	@Override
	public boolean isTraceable() {
		return true;
	}

	@Override
	public Collection<TraceableItemManager> getRegisteredAt() {
		return registeredAt;
	}

	@Override
	public boolean isRegisteredAt(TraceableItemManager manager) {
		return registeredAt.contains(manager);
	}

	@Override
	public boolean setRegisteredAt(TraceableItemManager manager, boolean registered) {
		if (registered) {
			return registeredAt.add(manager);
		} else {
			return registeredAt.remove(manager);
		}
	}
}

package com.zettelnet.magecraft.magic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.entity.Entity;

import com.zettelnet.magecraft.CustomEntity;
import com.zettelnet.magecraft.CustomType;
import com.zettelnet.magecraft.EntityManager;
import com.zettelnet.magecraft.TypeManager;
import com.zettelnet.magecraft.service.entity.EntitySpawnService;

public class MagicEntityManager implements EntityManager {

	private final TypeManager typeManager;
	private final Map<Entity, CustomEntity> entitiesByHandle;

	public MagicEntityManager(TypeManager typeManager) {
		this.typeManager = typeManager;
		this.entitiesByHandle = new HashMap<>();
	}

	@Override
	public Collection<CustomEntity> getRegisteredEntities() {
		return Collections.unmodifiableCollection(entitiesByHandle.values());
	}

	@Override
	public Collection<CustomEntity> getRegisteredEntities(CustomType type) {
		Collection<CustomEntity> list = new ArrayList<>();
		for (CustomEntity entity : entitiesByHandle.values()) {
			if (type.equals(entity.getType())) {
				list.add(entity);
			}
		}
		return Collections.unmodifiableCollection(list);
	}

	@Override
	public CustomEntity getEntityByHandle(Entity handle) {
		return entitiesByHandle.get(handle);
	}

	@Override
	public void registerEntities(Collection<CustomEntity> entities) {
		for (CustomEntity entity : entities) {
			unregisterEntity(entity);
		}
	}

	@Override
	public boolean registerEntity(CustomEntity entity) {
		if (isEntityRegistered(entity)) {
			return false;
		}
		entitiesByHandle.put(entity.getHandle(), entity);
		entity.setRegisteredAt(this, true);
		return true;
	}

	@Override
	public void unregisterAllEntities() {
		// create new list to avoid ConcurrentModificationException
		unregisterEntities(new ArrayList<>(entitiesByHandle.values()));
	}

	@Override
	public void unregisterEntities(Collection<CustomEntity> entities) {
		for (CustomEntity entity : entities) {
			unregisterEntity(entity);
		}
	}

	@Override
	public boolean unregisterEntity(CustomEntity entity) {
		if (!isEntityRegistered(entity)) {
			return false;
		}
		entity.setRegisteredAt(this, false);
		entitiesByHandle.remove(entity.getHandle());
		return true;
	}

	@Override
	public boolean isEntityRegistered(CustomEntity entity) {
		return entitiesByHandle.containsValue(entity);
	}

	@Override
	public boolean isEntityHandleRegistered(Entity handle) {
		return entitiesByHandle.containsKey(handle);
	}

	public CustomEntity loadEntity(CustomType type, Entity handle) {
		if (!typeManager.isTypeRegistered(type)) {
			throw new IllegalStateException("Entity type has to be registered");
		}
		CustomEntity entity = new MagicEntity(type, handle);
		registerEntity(entity);
		return entity;
	}

	public CustomEntity createEntity(CustomType type, Location location) {
		Entity handle = type.getService(EntitySpawnService.class).spawnEntity(type, location);
		return loadEntity(type, handle);
	}
}

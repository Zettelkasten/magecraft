package com.zettelnet.magecraft.magic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.zettelnet.magecraft.CustomType;
import com.zettelnet.magecraft.TypeManager;

public class MagicTypeManager implements TypeManager {

	private final Map<String, CustomType> typesByName;

	public MagicTypeManager() {
		this.typesByName = new HashMap<>();
	}

	@Override
	public CustomType getTypeByName(String name) {
		return typesByName.get(name);
	}

	@Override
	public Collection<CustomType> getRegisteredTypes() {
		return Collections.unmodifiableCollection(typesByName.values());
	}

	@Override
	public void registerTypes(Collection<CustomType> types) {
		for (CustomType type : types) {
			registerType(type);
		}
	}

	@Override
	public boolean registerType(CustomType type) {
		if (isTypeRegistered(type)) {
			return false;
		}
		typesByName.put(type.getName(), type);
		type.setRegisteredAt(this, true);
		return true;
	}

	@Override
	public void unregisterAllTypes() {
		// create new list to avoid ConcurrentModificationException
		unregisterTypes(new ArrayList<>(typesByName.values()));
	}

	@Override
	public void unregisterTypes(Collection<CustomType> types) {
		for (CustomType type : types) {
			unregisterType(type);
		}
	};

	@Override
	public boolean unregisterType(CustomType type) {
		if (!isTypeRegistered(type)) {
			return false;
		}
		type.setRegisteredAt(this, false);
		typesByName.remove(type.getName());
		return true;
	}

	@Override
	public boolean isTypeRegistered(CustomType type) {
		return typesByName.containsValue(type);
	}

	@Override
	public boolean isTypeNameRegistered(String typeName) {
		return typesByName.containsKey(typeName);
	}
}

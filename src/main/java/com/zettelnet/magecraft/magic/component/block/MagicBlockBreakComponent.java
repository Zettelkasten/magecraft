package com.zettelnet.magecraft.magic.component.block;

import java.util.Collection;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.event.Event;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import com.zettelnet.event.EventManager;
import com.zettelnet.magecraft.BlockManager;
import com.zettelnet.magecraft.CustomBlock;
import com.zettelnet.magecraft.component.ComponentBase;
import com.zettelnet.magecraft.event.annotation.BlockHandler;
import com.zettelnet.magecraft.service.block.BlockDropsService;

/**
 * A component for blocks that drops block drops items and unregisteres the
 * block from all {@link BlockManager}s. The {@link BlockDropsService} is called
 * to get the items to drop.
 * 
 * @author Zettelkasten
 *
 */
public class MagicBlockBreakComponent extends ComponentBase {

	public MagicBlockBreakComponent(EventManager<Event> eventManager) {
		super(eventManager);
	}

	@BlockHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onBlockBreak(BlockBreakEvent event, CustomBlock block) {
		// drop items
		if (event.getPlayer().getGameMode() != GameMode.CREATIVE) {
			BlockDropsService dropsService = block.getType().getService(BlockDropsService.class);
			Collection<ItemStack> drops = dropsService.getDrops(block, event.getPlayer().getItemInHand());
			World world = block.getLocation().getWorld();
			for (ItemStack item : drops) {
				world.dropItemNaturally(block.getLocation(), item);
			}
		}

		// unregister
		for (BlockManager manager : block.getRegisteredAt()) {
			manager.unregisterBlock(block);
		}

		// At this point of time, the Bukkit API does NOT support clearing the
		// drops of blocks. Therefore we have to cancel the event and drop the
		// items on our own.
		// THIS DOES BREAK LOGGING PLUGINS and is just a temporary fix until the
		// Bukkit API includes this feature.
		event.setCancelled(true);
		block.getHandle().setType(Material.AIR);
	}
}

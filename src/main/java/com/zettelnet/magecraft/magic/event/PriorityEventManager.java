package com.zettelnet.magecraft.magic.event;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.bukkit.event.Event;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.IllegalPluginAccessException;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;

import com.zettelnet.event.DistributionExecutor;
import com.zettelnet.event.EventDistributer;
import com.zettelnet.event.EventException;
import com.zettelnet.event.EventExecutor;
import com.zettelnet.event.EventManager;
import com.zettelnet.event.EventReceiver;
import com.zettelnet.event.EventTarget;

/**
 * Represents an EventExecutor listening to all events on a specific priority
 * for multiple Components. This way, an event is only registered once for all
 * Components that listen to it on the event priority.
 * <p>
 * Supports registering of single events unregistering of components, but not
 * registering of components and no calling of events. The parent event manager
 * can be used instead for these operations.
 * 
 * @author Zettelkasten
 *
 */
public class PriorityEventManager implements EventManager<Event>, org.bukkit.plugin.EventExecutor, DistributionExecutor<Event>, Listener {

	private final EventPriority priority;
	private final EventManager<Event> parent;

	private final Plugin plugin;
	private final PluginManager pluginManager;

	private static final Map<Class<? extends Event>, Class<? extends Event>> registrationClassCache = new HashMap<>();

	// Maps a receiver to the events it is listening to
	private final Map<EventReceiver, Map<Class<? extends Event>, Set<EventExecutor<Event>>>> receiverEvents;

	// All receiver executors that listen to a specific event.
	private final Map<Class<? extends Event>, Set<EventExecutor<Event>>> eventExecutors;

	public PriorityEventManager(final com.zettelnet.event.EventPriority priority, final EventManager<Event> parent, final Plugin plugin) {
		this.priority = EventPriority.values()[priority.getSlot()];
		this.parent = parent;

		this.plugin = plugin;
		this.pluginManager = plugin.getServer().getPluginManager();

		this.receiverEvents = new HashMap<>();
		this.eventExecutors = new HashMap<>();
	}

	@Override
	public void execute(Listener listener, Event event) {
		for (EventDistributer<Event> distributer : getDistributers()) {
			distributer.handleEvent(this, event);
		}
	}

	@Override
	public void executeEvent(Event event, Object recipient, EventTarget requiredTarget) {
		// call executors only for listeners with correct event class
		for (EventExecutor<Event> executor : eventExecutors.get(getRegistrationClass(event.getClass()))) {
			try {
				executor.execute(event, recipient, requiredTarget);
			} catch (EventException e) {
				e.printStackTrace();
			}
		}
	}

	protected void subscribeEvent(Class<? extends Event> event) {
		pluginManager.registerEvent(event, this, priority, this, plugin);
	}

	protected void unsubscribeEvent(Class<? extends Event> event) {
		try {
			Method method = event.getDeclaredMethod("getHandlerList");
			method.setAccessible(true);
			HandlerList handlers = (HandlerList) method.invoke(null);
			handlers.unregister(this);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new RuntimeException("Unable to find handler list for event " + event.getName(), e);
		}
	}

	@Override
	public void callEvent(Event event) throws UnsupportedOperationException {
		throw new UnsupportedOperationException("Cannot call events on priority event manager");
	}

	@Override
	public void registerEvents(Collection<EventReceiver> receivers) {
		for (EventReceiver receiver : receivers) {
			registerEvents(receiver);
		}
	}

	@Override
	public void registerEvents(EventReceiver receiver) {
		throw new UnsupportedOperationException("Cannot register all receiver events at once");
	}

	@Override
	public void registerEvent(EventExecutor<Event> executor) {
		Class<? extends Event> event = getRegistrationClass(executor.getEventType());
		EventReceiver receiver = executor.getReceiver();

		// if receiver has no event registered yet, create map entry for it
		if (!receiverEvents.containsKey(receiver)) {
			receiverEvents.put(receiver, new HashMap<Class<? extends Event>, Set<EventExecutor<Event>>>());
		}
		// if event type has not been registered yet, subscribe to the events
		// and create map entry
		if (!eventExecutors.containsKey(event)) {
			subscribeEvent(event);
			eventExecutors.put(event, new HashSet<EventExecutor<Event>>());
		}

		// add registered listener to event specific list
		eventExecutors.get(event).add(executor);

		Map<Class<? extends Event>, Set<EventExecutor<Event>>> componentListeners = receiverEvents.get(receiver);
		// if event type has not been registered for that receiver yet, create
		// map entry
		if (!componentListeners.containsKey(event)) {
			componentListeners.put(event, new HashSet<EventExecutor<Event>>());
		}

		// add registered listener to receiver specific list
		componentListeners.get(event).add(executor);
	}

	@Override
	public void unregisterAllEvents() {
		for (Class<? extends Event> event : eventExecutors.keySet()) {
			unsubscribeEvent(event);
		}
		receiverEvents.clear();
		eventExecutors.clear();
	}

	@Override
	public void unregisterEvents(Collection<EventReceiver> receivers) {
		for (EventReceiver receiver : receivers) {
			unregisterEvents(receiver);
		}
	}

	@Override
	public void unregisterEvents(EventReceiver receiver) {
		if (!receiverEvents.containsKey(receiver)) {
			return;
		}
		for (Set<EventExecutor<Event>> listener : receiverEvents.get(receiver).values()) {
			for (EventExecutor<Event> executor : listener) {
				for (Iterator<Map.Entry<Class<? extends Event>, Set<EventExecutor<Event>>>> i = eventExecutors.entrySet().iterator(); i.hasNext();) {
					Map.Entry<Class<? extends Event>, Set<EventExecutor<Event>>> entry = i.next();
					Set<EventExecutor<Event>> executors = entry.getValue();
					if (executors.remove(executor) && executors.isEmpty()) {
						// event subscription no longer needed
						i.remove();
						unsubscribeEvent(entry.getKey());
					}
				}
			}
		}
		receiverEvents.remove(receiver);
	}

	@Override
	public void unregisterEvent(EventExecutor<Event> executor) {
		EventReceiver receiver = executor.getReceiver();
		if (!receiverEvents.containsKey(receiver)) {
			return;
		}
		for (Iterator<Set<EventExecutor<Event>>> i = receiverEvents.get(receiver).values().iterator(); i.hasNext();) {
			Set<EventExecutor<Event>> listeners = i.next();
			if (listeners.remove(executor) && listeners.isEmpty()) {
				i.remove();
			}
		}
		if (receiverEvents.get(receiver).isEmpty()) {
			receiverEvents.remove(receiver);
		}
		for (Iterator<Map.Entry<Class<? extends Event>, Set<EventExecutor<Event>>>> i = eventExecutors.entrySet().iterator(); i.hasNext();) {
			Map.Entry<Class<? extends Event>, Set<EventExecutor<Event>>> entry = i.next();
			Set<EventExecutor<Event>> listeners = entry.getValue();
			if (listeners.remove(executor) && listeners.isEmpty()) {
				i.remove();
				unsubscribeEvent(entry.getKey());
			}
		}
	}

	@Override
	public boolean addDistributer(EventDistributer<Event> distributer) {
		return parent.addDistributer(distributer);
	}

	@Override
	public boolean removeDistributer(EventDistributer<Event> distributer) {
		return parent.removeDistributer(distributer);
	}

	@Override
	public Set<EventDistributer<Event>> getDistributers() {
		return parent.getDistributers();
	}

	private static Class<? extends Event> getRegistrationClass(Class<? extends Event> event) {
		if (registrationClassCache.containsKey(event)) {
			return registrationClassCache.get(event);
		}
		return getRegistrationClass(event, event);
	}

	// copied from SimplePluginManager#getRegistrationClass(Class<? extends
	// Event>)
	// used to get superclass of event class that contains the method
	// getHandlerList()
	private static Class<? extends Event> getRegistrationClass(Class<? extends Event> event, Class<? extends Event> clazz) {
		try {
			clazz.getDeclaredMethod("getHandlerList");
			registrationClassCache.put(event, clazz);
			return clazz;
		} catch (NoSuchMethodException e) {
			if (clazz.getSuperclass() != null
					&& !clazz.getSuperclass().equals(Event.class)
					&& Event.class.isAssignableFrom(clazz.getSuperclass())) {
				return getRegistrationClass(clazz.getSuperclass().asSubclass(Event.class));
			} else {
				throw new IllegalPluginAccessException("Unable to find handler list for event " + event.getName());
			}
		}
	}
}

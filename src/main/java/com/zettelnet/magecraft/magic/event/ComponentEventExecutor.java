package com.zettelnet.magecraft.magic.event;

import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;

import com.zettelnet.event.EventException;
import com.zettelnet.event.EventExecutor;
import com.zettelnet.event.EventPriority;
import com.zettelnet.event.EventReceiver;
import com.zettelnet.event.EventTarget;
import com.zettelnet.magecraft.CustomObject;
import com.zettelnet.magecraft.component.Component;
import com.zettelnet.magecraft.event.annotation.EventAnnotation;

public abstract class ComponentEventExecutor implements EventExecutor<Event> {

	private final Component component;

	private final Class<? extends Event> event;
	private final EventPriority priority;
	private final boolean ignoreCancelled;
	private final EventTarget target;

	public ComponentEventExecutor(final Component component, final Class<? extends Event> event, final EventAnnotation annotation) {
		this(component, event, annotation.priority(), annotation.ignoreCancelled(), annotation.target());
	}

	public ComponentEventExecutor(final Component component, final Class<? extends Event> event, final EventPriority priority, final boolean ignoreCancelled, final EventTarget target) {
		this.component = component;

		this.event = event;
		this.priority = priority;
		this.ignoreCancelled = ignoreCancelled;
		this.target = target;
	}

	@Override
	public Class<? extends Event> getEventType() {
		return event;
	}

	@Override
	public EventReceiver getReceiver() {
		return component;
	}

	@Override
	public EventPriority getPriority() {
		return priority;
	}

	public boolean isIgnoringCancelled() {
		return ignoreCancelled;
	}

	@Override
	public EventTarget getTarget() {
		return target;
	}

	@Override
	public boolean execute(Event event, Object recipient, EventTarget requiredTarget) throws EventException, IllegalArgumentException {
		if (!getEventType().isInstance(event)) {
			return false;
		}
		if (ignoreCancelled && event instanceof Cancellable && ((Cancellable) event).isCancelled()) {
			return false;
		}
		if (!this.target.isAssignableFrom(requiredTarget)) {
			return false;
		}
		if (!(recipient instanceof CustomObject)) {
			throw new IllegalArgumentException("Recipient must be a CustomObject");
		}
		// IMPORTANT: check that recipient has the specified component
		if (!((CustomObject) recipient).getType().containsComponent(component)) {
			return false;
		}

	perform(event, recipient);
		return true; // exception should be thrown on failure
	}

	protected abstract void perform(Event event, Object recipient) throws EventException;

}

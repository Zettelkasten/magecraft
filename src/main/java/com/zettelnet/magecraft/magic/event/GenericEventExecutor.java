package com.zettelnet.magecraft.magic.event;

import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;

import com.zettelnet.event.EventException;
import com.zettelnet.event.EventExecutor;
import com.zettelnet.event.EventPriority;
import com.zettelnet.event.EventReceiver;
import com.zettelnet.event.EventTarget;
import com.zettelnet.magecraft.event.annotation.EventAnnotation;

public abstract class GenericEventExecutor implements EventExecutor<Event> {

	private final EventReceiver receiver;

	private final Class<? extends Event> event;
	private final EventPriority priority;
	private final boolean ignoreCancelled;
	private final EventTarget target;

	public GenericEventExecutor(final EventReceiver receiver, final Class<? extends Event> event, final EventAnnotation annotation) {
		this(receiver, event, annotation.priority(), annotation.ignoreCancelled(), annotation.target());
	}

	public GenericEventExecutor(final EventReceiver receiver, final Class<? extends Event> event, final EventPriority priority, final boolean ignoreCancelled, final EventTarget target) {
		this.receiver = receiver;

		this.event = event;
		this.priority = priority;
		this.ignoreCancelled = ignoreCancelled;
		this.target = target;
	}

	@Override
	public Class<? extends Event> getEventType() {
		return event;
	}

	@Override
	public EventReceiver getReceiver() {
		return receiver;
	}

	@Override
	public EventPriority getPriority() {
		return priority;
	}

	public boolean isIgnoringCancelled() {
		return ignoreCancelled;
	}

	@Override
	public EventTarget getTarget() {
		return target;
	}

	// will always call perform(..) for all events that have correct event class
	// and are not cancelled
	// no check if required target is met
	@Override
	public boolean execute(Event event, Object recipient, EventTarget requiredTarget) throws EventException {
		if (!getEventType().isInstance(event)) {
			return false;
		}
		if (ignoreCancelled && event instanceof Cancellable && ((Cancellable) event).isCancelled()) {
			return false;
		}

		perform(event, recipient);
		return true; // exception should be thrown on failure
	}

	protected abstract void perform(Event event, Object recipient) throws EventException;

}

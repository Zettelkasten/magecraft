package com.zettelnet.magecraft.magic.event;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.plugin.Plugin;

import com.zettelnet.event.EventDistributer;
import com.zettelnet.event.EventException;
import com.zettelnet.event.EventExecutor;
import com.zettelnet.event.EventManager;
import com.zettelnet.event.EventPriority;
import com.zettelnet.event.EventReceiver;
import com.zettelnet.magecraft.CustomBlock;
import com.zettelnet.magecraft.CustomEntity;
import com.zettelnet.magecraft.CustomItem;
import com.zettelnet.magecraft.CustomObject;
import com.zettelnet.magecraft.component.Component;
import com.zettelnet.magecraft.event.annotation.BlockHandler;
import com.zettelnet.magecraft.event.annotation.EntityHandler;
import com.zettelnet.magecraft.event.annotation.EventAnnotation;
import com.zettelnet.magecraft.event.annotation.ItemHandler;

public class MagicEventManager implements EventManager<Event> {

	private final Map<EventPriority, EventManager<Event>> managers;

	private final Set<EventDistributer<Event>> distributers;

	public MagicEventManager(final Plugin plugin) {
		this.managers = new EnumMap<>(EventPriority.class);
		for (EventPriority priority : EventPriority.values()) {
			this.managers.put(priority, new PriorityEventManager(priority, this, plugin));
		}
		this.distributers = new HashSet<>();
	}

	@Override
	public void callEvent(Event event) {
		for (EventPriority priority : EventPriority.values()) {
			managers.get(priority).callEvent(event);
		}
	}

	@Override
	public void registerEvents(final Collection<EventReceiver> receivers) {
		for (EventReceiver comp : receivers) {
			registerEvents(comp);
		}
	}

	@Override
	public void registerEvents(final EventReceiver receiver) {
		Component component = (receiver instanceof Component) ? (Component) receiver : null;
		for (final Method method : receiver.getClass().getDeclaredMethods()) {
			// because annotations do not support inherence, the class
			// EventAnnotation is used to store annotation information
			for (Annotation annotation : method.getDeclaredAnnotations()) {
				Class<? extends Annotation> type = annotation.annotationType();
				if (type == EventHandler.class) {
					registerGenericEventMethod(receiver, method, EventAnnotation.fromEventHandler((EventHandler) annotation));
				} else {
					if (component == null) {
						// receiver class needs to be component to register
						// component specific types
						throw new RuntimeException(String.format("Event receiver needs to be an Component to register component secific events for method %s", method));
					}

					if (type == ItemHandler.class) {
						registerEventMethod(component, method, EventAnnotation.fromItemHandler((ItemHandler) annotation), CustomItem.class);
					} else if (type == BlockHandler.class) {
						registerEventMethod(component, method, EventAnnotation.fromBlockHandler((BlockHandler) annotation), CustomBlock.class);
					} else if (type == EntityHandler.class) {
						registerEventMethod(component, method, EventAnnotation.fromEntityHandler((EntityHandler) annotation), CustomEntity.class);
					}
				}

			}
		}
	}

	// registeres a method marked with @EventHandler
	protected void registerGenericEventMethod(final EventReceiver receiver, final Method method, final EventAnnotation annotation) {
		Class<?>[] types = method.getParameterTypes();
		if (types.length != 1) {
			throw new RuntimeException(String.format("Method %s needs to have on parameter", method));
		}
		Class<?> eventClass = types[0];
		if (!Event.class.isAssignableFrom(eventClass)) {
			throw new RuntimeException(String.format("Parameters of method %s need to be (Event)", method));
		}
		method.setAccessible(true);
		@SuppressWarnings("unchecked")
		Class<? extends Event> event = (Class<? extends Event>) eventClass;
		EventExecutor<Event> executor = new GenericEventExecutor(receiver, event, annotation) {
			@Override
			protected void perform(Event event, Object recipient) throws EventException {
				try {
					method.invoke(getReceiver(), event);
				} catch (Exception e) {
					throw new EventException(e);
				}
			}
		};
		registerEvent(executor);
	}

	// registeres a method marked with @ItemHandler, @BlockHandler or
	// @EntityHandler
	protected void registerEventMethod(final Component component, final Method method, final EventAnnotation annotation, final Class<? extends CustomObject> typeClass) {
		Class<?>[] types = method.getParameterTypes();
		if (types.length != 2) {
			throw new RuntimeException(String.format("Method %s needs to have two parameters", method));
		}
		Class<?> eventClass = types[0];
		Class<?> objClass = types[1];
		if (!Event.class.isAssignableFrom(eventClass) || !objClass.isAssignableFrom(typeClass)) {
			throw new RuntimeException(String.format("Parameters of method %s need to be (Event, CustomObject)", method));
		}
		method.setAccessible(true);
		@SuppressWarnings("unchecked")
		Class<? extends Event> event = (Class<? extends Event>) eventClass;
		EventExecutor<Event> executor = new ComponentEventExecutor(component, event, annotation) {
			@Override
			protected void perform(Event event, Object recipient) throws EventException {
				try {
					method.invoke(getReceiver(), event, recipient);
				} catch (Exception e) {
					throw new EventException(e);
				}
			}
		};
		registerEvent(executor);
	}

	@Override
	public void registerEvent(final EventExecutor<Event> executor) {
		managers.get(executor.getPriority()).registerEvent(executor);
	}

	@Override
	public void unregisterAllEvents() {
		for (EventManager<Event> manager : managers.values()) {
			manager.unregisterAllEvents();
		}
	}

	@Override
	public void unregisterEvents(final Collection<EventReceiver> receivers) {
		for (EventReceiver receiver : receivers) {
			unregisterEvents(receiver);
		}
	}

	@Override
	public void unregisterEvents(final EventReceiver receiver) {
		for (EventManager<Event> manager : managers.values()) {
			manager.unregisterEvents(receiver);
		}
	}

	@Override
	public void unregisterEvent(EventExecutor<Event> executor) {
		for (EventManager<Event> manager : managers.values()) {
			manager.unregisterEvent(executor);
		}
	}

	@Override
	public Set<EventDistributer<Event>> getDistributers() {
		return distributers;
	}

	@Override
	public boolean addDistributer(final EventDistributer<Event> distributer) {
		return distributers.add(distributer);
	}

	@Override
	public boolean removeDistributer(final EventDistributer<Event> distributer) {
		return distributers.remove(distributer);
	}
}

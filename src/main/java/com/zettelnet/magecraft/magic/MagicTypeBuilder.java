package com.zettelnet.magecraft.magic;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bukkit.plugin.Plugin;

import com.zettelnet.magecraft.CustomType;
import com.zettelnet.magecraft.CustomTypeBuilder;
import com.zettelnet.magecraft.component.Component;
import com.zettelnet.magecraft.service.Service;

public class MagicTypeBuilder implements CustomTypeBuilder {

	private Plugin plugin;
	private String name;

	private final Set<Component> components;
	private final Map<Class<? extends Service>, Service> services;

	private boolean enabled;

	public MagicTypeBuilder() {
		this.components = new HashSet<>();
		this.services = new HashMap<>();
	}

	public CustomTypeBuilder plugin(Plugin plugin) {
		this.plugin = plugin;
		return this;
	}

	public CustomTypeBuilder name(String name) {
		this.name = name;
		return this;
	}

	public CustomTypeBuilder component(Component comp) {
		this.components.add(comp);
		return this;
	}

	public final CustomTypeBuilder service(Service service, Class<? extends Service> task) throws IllegalArgumentException {
		Class<? extends Service> serviceClass = service.getClass();
		if (!task.isAssignableFrom(serviceClass)) {
			throw new IllegalArgumentException("Service does not implement task class");
		}
		this.services.put(task, service);
		return this;
	}

	public final CustomTypeBuilder service(Service service, Collection<Class<? extends Service>> tasks) throws IllegalArgumentException {
		Class<? extends Service> serviceClass = service.getClass();
		for (Class<? extends Service> task : tasks) {
			if (!task.isAssignableFrom(serviceClass)) {
				throw new IllegalArgumentException("Service does not implement task class");
			}
			this.services.put(task, service);
		}
		return this;
	}

	public CustomTypeBuilder enabled(boolean enabled) {
		this.enabled = enabled;
		return this;
	}

	public boolean isReady() {
		return plugin != null && name != null;
	}

	public CustomType build() throws IllegalStateException {
		if (!isReady()) {
			throw new IllegalStateException("Builder missing information");
		}
		CustomType type = new MagicType(plugin, name);
		type.addComponents(components);
		type.addServices(services);
		type.setEnabled(enabled);
		return type;
	}
}

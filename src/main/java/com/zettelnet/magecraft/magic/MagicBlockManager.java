package com.zettelnet.magecraft.magic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.block.Block;

import com.zettelnet.magecraft.BlockManager;
import com.zettelnet.magecraft.CustomBlock;
import com.zettelnet.magecraft.CustomType;
import com.zettelnet.magecraft.TypeManager;
import com.zettelnet.magecraft.service.block.BlockPlacementService;

public class MagicBlockManager implements BlockManager {

	private final TypeManager typeManager;
	private final Map<Block, CustomBlock> blocksByHandle;

	public MagicBlockManager(TypeManager typeManager) {
		this.typeManager = typeManager;
		this.blocksByHandle = new HashMap<>();
	}

	@Override
	public Collection<CustomBlock> getRegisteredBlocks() {
		return Collections.unmodifiableCollection(blocksByHandle.values());
	}

	@Override
	public Collection<CustomBlock> getRegisteredBlocks(CustomType type) {
		Collection<CustomBlock> list = new ArrayList<>();
		for (CustomBlock block : blocksByHandle.values()) {
			if (type.equals(block.getType())) {
				list.add(block);
			}
		}
		return Collections.unmodifiableCollection(list);
	}

	@Override
	public CustomBlock getBlockByHandle(Block handle) {
		return blocksByHandle.get(handle);
	}

	@Override
	public CustomBlock getBlockByLocation(Location location) {
		return getBlockByHandle(location.getBlock());
	}

	@Override
	public void registerBlocks(Collection<CustomBlock> blocks) {
		for (CustomBlock block : blocks) {
			registerBlock(block);
		}
	};

	@Override
	public boolean registerBlock(CustomBlock block) {
		if (isBlockRegistered(block)) {
			return false;
		}
		blocksByHandle.put(block.getHandle(), block);
		block.setRegisteredAt(this, true);
		return true;
	}

	@Override
	public void unregisterAllBlocks() {
		// create new list to avoid ConcurrentModificationException
		unregisterBlocks(new ArrayList<>(blocksByHandle.values()));
	}

	@Override
	public void unregisterBlocks(Collection<CustomBlock> blocks) {
		for (CustomBlock block : blocks) {
			unregisterBlock(block);
		}
	}

	@Override
	public boolean unregisterBlock(CustomBlock block) {
		if (!isBlockRegistered(block)) {
			return false;
		}
		block.setRegisteredAt(this, false);
		blocksByHandle.remove(block.getHandle());
		return true;
	}

	@Override
	public boolean isBlockRegistered(CustomBlock block) {
		return blocksByHandle.containsValue(block);
	}

	@Override
	public boolean isBlockHandleRegistered(Block handle) {
		return blocksByHandle.containsKey(handle);
	}

	@Override
	public boolean isBlockLocationRegistered(Location location) {
		return isBlockHandleRegistered(location.getBlock());
	}

	public CustomBlock loadBlock(CustomType type, Block handle) {
		if (!typeManager.isTypeRegistered(type)) {
			throw new IllegalStateException("Block type has to be registered");
		}
		CustomBlock block = new MagicBlock(type, handle);
		registerBlock(block);
		return block;
	}

	public CustomBlock createBlock(CustomType type, Block handle) {
		CustomBlock block = loadBlock(type, handle);
		BlockPlacementService placement = type.getService(BlockPlacementService.class);
		placement.setBlock(block);
		return block;
	}

	public void destroyBlock(CustomBlock block) {
		BlockPlacementService placement = block.getType().getService(BlockPlacementService.class);
		placement.breakBlock(block);
	}
}

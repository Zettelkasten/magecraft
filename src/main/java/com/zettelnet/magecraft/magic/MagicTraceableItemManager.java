package com.zettelnet.magecraft.magic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.zettelnet.magecraft.CustomItem;
import com.zettelnet.magecraft.CustomType;
import com.zettelnet.magecraft.TraceableItem;
import com.zettelnet.magecraft.TraceableItemManager;
import com.zettelnet.magecraft.TypeManager;
import com.zettelnet.magecraft.service.item.ItemCreatingService;
import com.zettelnet.magecraft.service.item.UniqueItemCreatingService;

public class MagicTraceableItemManager implements TraceableItemManager {

	private final TypeManager typeManager;
	private final Map<UUID, TraceableItem> itemsByUniqueId;

	public MagicTraceableItemManager(TypeManager typeManager) {
		this.typeManager = typeManager;
		this.itemsByUniqueId = new HashMap<>();
	}

	@Override
	public Collection<TraceableItem> getRegisteredItems() {
		return Collections.unmodifiableCollection(itemsByUniqueId.values());
	}

	@Override
	public Collection<TraceableItem> getRegisteredItems(CustomType type) {
		Collection<TraceableItem> list = new ArrayList<>();
		for (TraceableItem item : itemsByUniqueId.values()) {
			if (type.equals(item.getType())) {
				list.add(item);
			}
		}
		return Collections.unmodifiableCollection(list);
	}

	@Override
	public CustomItem getItemByHandle(ItemStack handle, Inventory inventory) {
		if (handle == null || !handle.hasItemMeta()) {
			return null;
		}
		ItemMeta meta = handle.getItemMeta();

		if (meta.hasLore()) {
			String lore = meta.getLore().get(0);
			try {
				TraceableItem item = getItemByUniqueId(UUID.fromString(lore));
				if (item != null) {
					item.updateHandle(handle);
					return item;
				}
			} catch (IllegalArgumentException e) {
			}
		}

		if (meta.hasDisplayName()) {
			String name = meta.getDisplayName();
			CustomType type = typeManager.getTypeByName(name);
			if (type != null) {
				return loadItem(type, handle, inventory);
			}
		}

		return null;
	}

	@Override
	public TraceableItem getItemByUniqueId(UUID uniqueId) {
		return itemsByUniqueId.get(uniqueId);
	}

	@Override
	public void registerItems(Collection<TraceableItem> items) {
		for (TraceableItem item : items) {
			registerItem(item);
		}
	}

	@Override
	public boolean registerItem(TraceableItem item) {
		if (isItemRegistered(item)) {
			return false;
		}
		itemsByUniqueId.put(item.getUniqueId(), item);
		item.setRegisteredAt(this, true);
		return true;
	}

	@Override
	public void unregisterAllItems() {
		// create new list to avoid ConcurrentModificationException
		unregisterItems(new ArrayList<>(itemsByUniqueId.values()));
	}

	@Override
	public void unregisterItems(Collection<TraceableItem> items) {
		for (TraceableItem item : items) {
			unregisterItem(item);
		}
	}

	@Override
	public boolean isItemRegistered(TraceableItem item) {
		return itemsByUniqueId.containsValue(item);
	}

	@Override
	public boolean isItemUniqueIdRegistered(UUID uniqueId) {
		return itemsByUniqueId.containsKey(uniqueId);
	}

	@Override
	public boolean unregisterItem(TraceableItem item) {
		if (!isItemRegistered(item)) {
			return false;
		}
		item.setRegisteredAt(this, false);
		itemsByUniqueId.remove(item.getUniqueId());
		return true;
	}

	public CustomItem loadItem(CustomType type, ItemStack handle, Inventory inventory) {
		if (!typeManager.isTypeRegistered(type)) {
			throw new IllegalStateException("Item type has to be registered");
		}
		CustomItem item = new MagicItem(type, handle, inventory);
		return item;
	}

	public CustomItem createItem(CustomType type, int amount, Inventory inventory) {
		ItemStack handle = type.getService(ItemCreatingService.class).createItem(type, amount);
		return loadItem(type, handle, inventory);
	}

	public TraceableItem loadUniqueItem(CustomType type, UUID uniqueId, ItemStack handle, Inventory inventory) {
		if (!typeManager.isTypeRegistered(type)) {
			throw new IllegalStateException("Item type has to be registered");
		}
		TraceableItem item = new MagicTraceableItem(type, uniqueId, handle, inventory);
		registerItem(item);
		return item;
	}

	public TraceableItem createTraceableItem(CustomType type, Inventory inventory) {
		UUID uniqueId = UUID.randomUUID();
		ItemStack handle = type.getService(UniqueItemCreatingService.class).createItem(type, uniqueId);
		return loadUniqueItem(type, uniqueId, handle, inventory);
	}

}

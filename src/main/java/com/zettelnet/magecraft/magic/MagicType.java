package com.zettelnet.magecraft.magic;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bukkit.Server;
import org.bukkit.plugin.Plugin;

import com.zettelnet.magecraft.CustomType;
import com.zettelnet.magecraft.Magecraft;
import com.zettelnet.magecraft.TypeManager;
import com.zettelnet.magecraft.component.Component;
import com.zettelnet.magecraft.service.Service;
import com.zettelnet.magecraft.service.ServiceContainer;
import com.zettelnet.magecraft.service.ServiceMap;
import com.zettelnet.magecraft.service.ServiceUnavailableException;

public class MagicType implements CustomType {

	private final Plugin plugin;
	private final String name;

	private final Set<Component> components;
	private final ServiceContainer services;

	private final Set<TypeManager> registeredAt;

	private boolean enabled;

	public MagicType(final Plugin plugin, final String name) {
		this.plugin = plugin;
		this.name = name;

		this.components = new ComponentSet();
		this.services = new ServiceMap(Magecraft.getInstace().getServiceDefaults());

		this.registeredAt = new HashSet<>(1);

		this.enabled = false;
	}

	@Override
	public Plugin getPlugin() {
		return plugin;
	}

	@Override
	public Server getServer() {
		return plugin.getServer();
	}

	@Override
	public String getName() {
		return name;
	}

	// Components

	private class ComponentSet extends com.zettelnet.magecraft.component.ComponentSet {
		@Override
		public boolean add(Component component) throws NullPointerException {
			if (!super.add(component)) {
				return false;
			}
			if (isEnabled()) {
				component.setRegisteredAt(MagicType.this, true);
				component.setEnabled(true);
			}
			return true;
		}
	}

	@Override
	public Set<Component> getComponents() {
		return components;
	}

	@Override
	public boolean addComponents(Collection<Component> components) {
		return this.components.addAll(components);
	}

	@Override
	public boolean addComponent(Component component) {
		return components.add(component);
	}

	@Override
	public boolean removeComponents(Collection<Component> components) {
		return this.components.removeAll(components);
	}

	@Override
	public boolean removeComponent(Component component) {
		return components.remove(component);
	}

	@Override
	public boolean containsComponent(Component component) {
		return components.contains(component);
	}

	// Services

	@Override
	public Collection<Service> getServices() {
		return services.getServices();
	}

	@Override
	public Map<Class<? extends Service>, Service> getServicesTasks() {
		return services.getServicesTasks();
	}

	@Override
	public <T extends Service> T getService(Class<T> task) throws ServiceUnavailableException {
		return services.getService(task);
	}

	@Override
	public Service addService(Service service, Class<? extends Service> task) throws IllegalArgumentException {
		return services.addService(service, task);
	}

	@Override
	public void addService(Service service, Collection<Class<? extends Service>> tasks) throws IllegalArgumentException {
		services.addService(service, tasks);
	}

	@Override
	public void addServices(Map<Class<? extends Service>, Service> services) throws IllegalArgumentException {
		this.services.addServices(services);
	}

	@Override
	public void removeService(Service service) {
		services.removeService(service);
	}

	@Override
	public void removeServices(Collection<Service> services) {
		this.services.removeServices(services);
	}

	@Override
	public Service removeServiceTask(Class<? extends Service> task) {
		return services.removeServiceTask(task);
	}

	@Override
	public void removeServiceTasks(Collection<Class<? extends Service>> tasks) {
		services.removeServiceTasks(tasks);
	}

	@Override
	public boolean containsService(Service service) {
		return services.containsService(service);
	}

	@Override
	public boolean containsServiceTask(Class<? extends Service> task) {
		return services.containsServiceTask(task);
	}

	@Override
	public ServiceContainer getBackingServiceContainer() {
		return services.getBackingServiceContainer();
	}

	@Override
	public ServiceContainer setBackingServiceContainer(ServiceContainer container) {
		return services.setBackingServiceContainer(container);
	}

	@Override
	public boolean hasBackingServiceContainer() {
		return services.hasBackingServiceContainer();
	}
	
	// Enabled

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public boolean setEnabled(boolean enabled) {
		if (this.enabled == enabled) {
			return false;
		}
		if (enabled) {
			enable();
		} else {
			disable();
		}
		this.enabled = enabled;
		return true;
	}

	protected void enable() {
		for (Component component : components) {
			component.setRegisteredAt(this, true);
			component.setEnabled(true);
		}
	}

	protected void disable() {
		for (Component component : components) {
			component.setRegisteredAt(this, false);
			if (component.getRegisteredAt().isEmpty()) {
				component.setEnabled(false);
			}
		}
	}
	
	// RegisteredAt

	@Override
	public Collection<TypeManager> getRegisteredAt() {
		return registeredAt;
	}

	@Override
	public boolean isRegisteredAt(TypeManager manager) {
		return registeredAt.contains(manager);
	}

	@Override
	public boolean setRegisteredAt(TypeManager manager, boolean registered) {
		if (registered) {
			return registeredAt.add(manager);
		} else {
			return registeredAt.remove(manager);
		}
	}
}

package com.zettelnet.magecraft.magic;

import org.bukkit.Server;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import com.zettelnet.magecraft.CustomItem;
import com.zettelnet.magecraft.CustomType;
import com.zettelnet.magecraft.data.DataContainer;
import com.zettelnet.magecraft.data.DataMap;

public class MagicItem implements CustomItem {

	private final CustomType type;
	private final ItemStack handle;
	private final Inventory inventory;

	private final DataContainer data;
	private final DataContainer transientData;

	public MagicItem(final CustomType type, final ItemStack handle, final Inventory inventory) {
		this.type = type;
		this.handle = handle;
		this.inventory = inventory;

		this.data = new DataMap();
		this.transientData = new DataMap();
	}

	@Override
	public CustomType getType() {
		return type;
	}

	@Override
	public Plugin getPlugin() {
		return type.getPlugin();
	}

	@Override
	public Server getServer() {
		return type.getServer();
	}

	@Override
	public DataContainer getData() {
		return data;
	}

	@Override
	public DataContainer getTransientData() {
		return transientData;
	}

	@Override
	public ItemStack getHandle() {
		return handle;
	}

	@Override
	public Inventory getInventory() {
		return inventory;
	}

	@Override
	public boolean isTraceable() {
		return false;
	}
}

package com.zettelnet.magecraft.magic;

import java.util.Collection;
import java.util.HashSet;

import org.bukkit.Server;
import org.bukkit.entity.Entity;
import org.bukkit.plugin.Plugin;

import com.zettelnet.magecraft.CustomEntity;
import com.zettelnet.magecraft.CustomType;
import com.zettelnet.magecraft.EntityManager;
import com.zettelnet.magecraft.data.DataContainer;
import com.zettelnet.magecraft.data.DataMap;

public class MagicEntity implements CustomEntity {

	private final CustomType type;
	private final Entity handle;

	private final Collection<EntityManager> registeredAt;

	private final DataContainer data;
	private final DataContainer transientData;

	public MagicEntity(final CustomType type, final Entity handle) {
		this.type = type;
		this.handle = handle;

		this.registeredAt = new HashSet<>(1);

		this.data = new DataMap();
		this.transientData = new DataMap();
	}

	@Override
	public CustomType getType() {
		return type;
	}

	@Override
	public Plugin getPlugin() {
		return type.getPlugin();
	}

	@Override
	public Server getServer() {
		return type.getServer();
	}

	@Override
	public Entity getHandle() {
		return handle;
	}

	@Override
	public DataContainer getData() {
		return data;
	}

	@Override
	public DataContainer getTransientData() {
		return transientData;
	}

	public Collection<EntityManager> getRegisteredAt() {
		return registeredAt;
	}

	public boolean isRegisteredAt(EntityManager manager) {
		return registeredAt.contains(manager);
	}

	public boolean setRegisteredAt(EntityManager manager, boolean registered) {
		if (registered) {
			return registeredAt.add(manager);
		} else {
			return registeredAt.remove(manager);
		}
	}
}

package com.zettelnet.magecraft.magic;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.block.Block;
import org.bukkit.plugin.Plugin;

import com.zettelnet.magecraft.BlockManager;
import com.zettelnet.magecraft.CustomBlock;
import com.zettelnet.magecraft.CustomType;
import com.zettelnet.magecraft.data.DataContainer;
import com.zettelnet.magecraft.data.DataMap;

public class MagicBlock implements CustomBlock {

	private final CustomType type;
	private final Block handle;

	private final Set<BlockManager> registeredAt;

	private final DataContainer data;
	private final DataContainer transientData;

	public MagicBlock(final CustomType type, final Block handle) {
		this.type = type;
		this.handle = handle;

		this.registeredAt = new HashSet<>(1);

		this.data = new DataMap();
		this.transientData = new DataMap();
	}

	@Override
	public CustomType getType() {
		return type;
	}

	@Override
	public Plugin getPlugin() {
		return type.getPlugin();
	}

	@Override
	public Server getServer() {
		return type.getServer();
	}

	@Override
	public DataContainer getData() {
		return data;
	}

	@Override
	public DataContainer getTransientData() {
		return transientData;
	}

	@Override
	public Block getHandle() {
		return handle;
	}

	@Override
	public Location getLocation() {
		return handle.getLocation();
	}

	@Override
	public Collection<BlockManager> getRegisteredAt() {
		return registeredAt;
	}

	@Override
	public boolean isRegisteredAt(BlockManager manager) {
		return registeredAt.contains(manager);
	}

	@Override
	public boolean setRegisteredAt(BlockManager manager, boolean registered) {
		if (registered) {
			return registeredAt.add(manager);
		} else {
			return registeredAt.remove(manager);
		}
	}
}

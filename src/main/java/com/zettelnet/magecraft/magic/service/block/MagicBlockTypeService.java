package com.zettelnet.magecraft.magic.service.block;

import org.bukkit.Material;
import org.bukkit.material.MaterialData;

import com.zettelnet.magecraft.service.block.BlockTypeService;

public class MagicBlockTypeService implements BlockTypeService {

	private final MaterialData material;
	private final MaterialData placingMaterial;

	@SuppressWarnings("deprecation")
	public MagicBlockTypeService(Material material, byte rawData) {
		this(new MaterialData(material, rawData), new MaterialData(material, rawData));
	}

	public MagicBlockTypeService(MaterialData material, MaterialData placingMaterial) {
		this.material = material;
		this.placingMaterial = placingMaterial;
	}

	@Override
	public MaterialData getMaterial() {
		return material;
	}

	@Override
	public MaterialData getPlacingMaterial() {
		return placingMaterial;
	}
}

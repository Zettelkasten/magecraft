package com.zettelnet.magecraft.magic.service.block;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.zettelnet.magecraft.CustomBlock;
import com.zettelnet.magecraft.CustomItem;
import com.zettelnet.magecraft.service.block.BlockDropsService;

public class MagicBlockDropsService implements BlockDropsService {

	private final Collection<ItemStack> drops;
	private final boolean anyTool;
	private final Set<Material> usableTools;

	public MagicBlockDropsService(Collection<ItemStack> drops, Collection<Material> usableTools) {
		this(drops, usableTools.toArray(new Material[usableTools.size()]));
	}

	public MagicBlockDropsService(Collection<ItemStack> drops, Material... usableTools) {
		this.drops = drops;
		if (usableTools.length == 0) {
			this.anyTool = true;
			this.usableTools = null;
		} else {
			this.anyTool = false;
			this.usableTools = new HashSet<Material>(Arrays.asList(usableTools));
		}
	}

	@Override
	public Collection<ItemStack> getDrops(CustomBlock block) {
		return new ArrayList<>(drops);
	}

	@Override
	public Collection<ItemStack> getDrops(CustomBlock block, ItemStack tool) {
		if (anyTool || usableTools.contains(tool.getType())) {
			return getDrops(block);
		} else {
			return new ArrayList<>();
		}
	}

	@Override
	public Collection<ItemStack> getDrops(CustomBlock block, CustomItem tool) {
		return getDrops(block, tool.getHandle());
	}

}

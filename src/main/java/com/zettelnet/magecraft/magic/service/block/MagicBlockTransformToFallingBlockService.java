package com.zettelnet.magecraft.magic.service.block;

import org.bukkit.Location;
import org.bukkit.entity.FallingBlock;

import com.zettelnet.magecraft.CustomBlock;
import com.zettelnet.magecraft.service.block.BlockTransformToFallingBlockService;
import com.zettelnet.magecraft.service.block.BlockTypeService;

public class MagicBlockTransformToFallingBlockService implements BlockTransformToFallingBlockService {

	@SuppressWarnings("deprecation")
	@Override
	public FallingBlock transformToFallingBlock(CustomBlock block) {
		BlockTypeService typeService = block.getType().getService(BlockTypeService.class);

		Location loc = block.getLocation();
		return loc.getWorld().spawnFallingBlock(loc, typeService.getMaterial().getItemType(), typeService.getMaterial().getData());
	}
}

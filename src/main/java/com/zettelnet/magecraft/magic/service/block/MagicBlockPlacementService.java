package com.zettelnet.magecraft.magic.service.block;

import java.util.Collection;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;

import com.zettelnet.magecraft.CustomBlock;
import com.zettelnet.magecraft.CustomItem;
import com.zettelnet.magecraft.service.block.BlockDropsService;
import com.zettelnet.magecraft.service.block.BlockPlacementService;
import com.zettelnet.magecraft.service.block.BlockTypeService;

public class MagicBlockPlacementService implements BlockPlacementService {

	private boolean applyPhysics;
	private boolean spawnItemsNaturally;

	public MagicBlockPlacementService() {
		this(true, true);
	}

	public MagicBlockPlacementService(boolean applyPhysics, boolean spawnItemsNaturally) {
		this.applyPhysics = applyPhysics;
		this.spawnItemsNaturally = spawnItemsNaturally;
	}

	@Override
	public boolean placeBlock(CustomBlock block) {
		Block handle = block.getHandle();
		if (!handle.isEmpty()) {
			return false;
		}
		return setBlock(block);
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean setBlock(CustomBlock block) {
		BlockTypeService typeService = block.getType().getService(BlockTypeService.class);
		MaterialData placingMaterial = typeService.getPlacingMaterial();
		block.getHandle().setType(placingMaterial.getItemType(), applyPhysics);
		block.getHandle().setData(placingMaterial.getData(), applyPhysics);
		return true;
	}

	@Override
	public boolean breakBlock(CustomBlock block) {
		BlockDropsService dropsService = block.getType().getService(BlockDropsService.class);
		return breakBlock(block, dropsService.getDrops(block));
	}

	@Override
	public boolean breakBlock(CustomBlock block, ItemStack tool) {
		BlockDropsService dropsService = block.getType().getService(BlockDropsService.class);
		return breakBlock(block, dropsService.getDrops(block, tool));
	}

	@Override
	public boolean breakBlock(CustomBlock block, CustomItem tool) {
		BlockDropsService dropsService = block.getType().getService(BlockDropsService.class);
		return breakBlock(block, dropsService.getDrops(block, tool));
	}

	private boolean breakBlock(CustomBlock block, Collection<ItemStack> drops) {
		Block handle = block.getHandle();
		if (handle.isEmpty()) {
			return false;
		}
		if (!removeBlock(block)) {
			return false;
		}
		for (ItemStack item : drops) {
			if (spawnItemsNaturally) {
				handle.getWorld().dropItemNaturally(handle.getLocation().add(0.5, 0.5, 0.5), item);
			} else {
				handle.getWorld().dropItem(handle.getLocation().add(0.5, 0.5, 0.5), item);
			}
		}
		return true;
	}

	@Override
	public boolean removeBlock(CustomBlock block) {
		block.getHandle().setType(Material.AIR);
		return true;
	}

}

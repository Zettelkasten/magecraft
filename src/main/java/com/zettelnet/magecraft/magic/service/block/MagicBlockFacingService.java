package com.zettelnet.magecraft.magic.service.block;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.material.Directional;
import org.bukkit.material.MaterialData;

import com.zettelnet.magecraft.CustomBlock;
import com.zettelnet.magecraft.service.block.BlockFacingService;

public class MagicBlockFacingService implements BlockFacingService {

	@Override
	public BlockFace getFacing(CustomBlock block) {
		Block handle = block.getHandle();
		MaterialData materialData = handle.getState().getData();

		if (materialData instanceof Directional) {
			Directional data = (Directional) materialData;
			return data.getFacing();
		}

		return BlockFace.SELF;
	}

	@SuppressWarnings("deprecation")
	@Override
	public BlockFace setFacing(CustomBlock block, BlockFace facing) {
		BlockFace prev = getFacing(block);

		Block handle = block.getHandle();
		MaterialData materialData = handle.getState().getData();

		if (materialData instanceof Directional) {
			Directional data = (Directional) materialData;
			data.setFacingDirection(facing);
		}
		handle.setData(materialData.getData());

		return prev;
	}

}

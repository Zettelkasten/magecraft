package com.zettelnet.magecraft.magic.service.item;

import java.util.Arrays;
import java.util.List;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.zettelnet.magecraft.CustomItem;
import com.zettelnet.magecraft.service.item.ItemLoreService;

public class MagicItemLoreService implements ItemLoreService {

	@Override
	public List<String> getLore(CustomItem item) {
		if (!hasLore(item)) {
			return null;
		}
		ItemMeta meta = item.getHandle().getItemMeta();
		return meta.getLore();
	}

	@Override
	public boolean hasLore(CustomItem item) {
		ItemStack handle = item.getHandle();
		if (!handle.hasItemMeta()) {
			return false;
		}
		ItemMeta meta = handle.getItemMeta();
		return meta.hasLore();
	}

	@Override
	public void setLore(CustomItem item, List<String> lore) {
		ItemMeta meta = item.getHandle().getItemMeta();
		meta.setLore(lore);
	}

	@Override
	public void setLore(CustomItem item, String... lore) {
		setLore(item, Arrays.asList(lore));
	}

}

package com.zettelnet.magecraft.data;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.bukkit.configuration.file.FileConfiguration;

public class FileConfigurationDataContainer implements DataContainer {

	private final FileConfiguration config;

	public FileConfigurationDataContainer(FileConfiguration config) {
		this.config = config;
	}

	@Override
	public boolean containsKey(String key) {
		return config.contains(key);
	}

	@Override
	public Object get(String key) {
		return config.get(key);
	}

	@Override
	public <T> T get(String key, Class<T> clazz) {
		return clazz.cast(get(key));
	}

	@Override
	public Object getOrDefault(String key, Object def) {
		return config.get(key, def);
	}

	@Override
	public <T> T getOrDefault(String key, Class<T> clazz, T def) {
		return clazz.cast(getOrDefault(key, def));
	}

	@Override
	public byte getByte(String key) {
		Byte i = get(key, Byte.class);
		if (i == null) {
			return 0;
		}
		return i;
	}

	@Override
	public short getShort(String key) {
		Short i = get(key, Short.class);
		if (i == null) {
			return 0;
		}
		return i;
	}

	@Override
	public int getInt(String key) {
		Integer i = get(key, Integer.class);
		if (i == null) {
			return 0;
		}
		return i;
	}

	@Override
	public long getLong(String key) {
		Long i = get(key, Long.class);
		if (i == null) {
			return 0;
		}
		return i;
	}

	@Override
	public float getFloat(String key) {
		Float d = get(key, Float.class);
		if (d == null) {
			return 0;
		}
		return d;
	}

	@Override
	public double getDouble(String key) {
		Double d = get(key, Double.class);
		if (d == null) {
			return 0;
		}
		return d;
	}

	@Override
	public boolean getBoolean(String key) {
		Boolean b = get(key, Boolean.class);
		if (b == null) {
			return false;
		}
		return b;
	}

	@Override
	public char getChar(String key) {
		Character c = get(key, Character.class);
		if (c == null) {
			return 0;
		}
		return c;
	}

	@Override
	public DataContainer getStorage(String key) {
		Object obj = get(key);
		if (obj instanceof FileConfiguration) {
			return new FileConfigurationDataContainer((FileConfiguration) obj);
		}
		return null;
	}

	@Override
	public Object put(String key, Object value) {
		Object obj = get(key);
		config.set(key, value);
		return obj;
	}

	@Override
	public void putAll(Map<String, Object> values) {
		for (Entry<String, Object> entry : values.entrySet()) {
			put(entry.getKey(), entry.getValue());
		}
	}

	@Override
	public Object remove(String key) {
		return put(key, null);
	}

	@Override
	public void clear() {
		for (String key : config.getKeys(false)) {
			remove(key);
		}
	}

	@Override
	public Set<String> keySet() {
		return keySet(false);
	}

	@Override
	public Set<String> keySet(boolean deep) {
		return config.getKeys(deep);
	}
}

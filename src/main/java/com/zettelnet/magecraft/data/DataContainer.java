package com.zettelnet.magecraft.data;

import java.util.Map;
import java.util.Set;

public interface DataContainer {

	boolean containsKey(String key);

	Object get(String key);

	<T> T get(String key, Class<T> clazz);

	Object getOrDefault(String key, Object def);

	<T> T getOrDefault(String key, Class<T> clazz, T def);

	byte getByte(String key);

	short getShort(String key);

	int getInt(String key);

	long getLong(String key);

	float getFloat(String key);

	double getDouble(String key);

	boolean getBoolean(String key);

	char getChar(String key);

	DataContainer getStorage(String key);

	Object put(String key, Object value);

	void putAll(Map<String, Object> values);

	Object remove(String key);

	void clear();

	Set<String> keySet();

	Set<String> keySet(boolean deep);
}

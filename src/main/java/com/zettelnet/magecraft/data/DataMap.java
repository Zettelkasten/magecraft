package com.zettelnet.magecraft.data;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DataMap implements DataContainer {

	private final Map<String, Object> base;

	public DataMap() {
		this.base = new HashMap<>();
	}

	@Override
	public boolean containsKey(String key) {
		String[] split = key.split(".", 2);
		if (split.length == 1) {
			return base.containsKey(split[0]);
		} else {
			DataContainer storage = getStorage(split[0]);
			if (storage == null) {
				return false;
			}
			return storage.containsKey(split[1]);
		}
	}

	@Override
	public Object get(String key) {
		return getOrDefault(key, null);
	}

	@Override
	public <T> T get(String key, Class<T> clazz) {
		return getOrDefault(key, clazz, null);
	}

	@Override
	public Object getOrDefault(String key, Object def) {
		String[] split = key.split("\\.", 2);
		if (split.length == 1) {
			if (base.containsKey(split[0])) {
				return base.get(split[0]);
			} else {
				return def;
			}
		} else {
			DataContainer storage = getStorage(split[0]);
			if (storage == null) {
				return def;
			}
			return storage.get(split[1]);
		}
	}

	@Override
	public <T> T getOrDefault(String key, Class<T> clazz, T def) {
		String[] split = key.split("\\.", 2);
		if (split.length == 1) {
			Object obj = base.get(split[0]);
			if (clazz.isInstance(key)) {
				return clazz.cast(obj);
			} else {
				return def;
			}
		} else {
			DataContainer storage = getStorage(split[0]);
			if (storage == null) {
				return def;
			}
			return storage.get(split[1], clazz);
		}
	}

	@Override
	public byte getByte(String key) {
		return getOrDefault(key, Byte.class, (byte) 0);
	}

	@Override
	public short getShort(String key) {
		return getOrDefault(key, Short.class, (short) 0);
	}

	@Override
	public int getInt(String key) {
		return getOrDefault(key, Integer.class, 0);
	}

	@Override
	public long getLong(String key) {
		return getOrDefault(key, Long.class, 0L);
	}

	@Override
	public float getFloat(String key) {
		return getOrDefault(key, Float.class, 0F);
	}

	@Override
	public double getDouble(String key) {
		return getOrDefault(key, Double.class, 0D);
	}

	@Override
	public boolean getBoolean(String key) {
		return getOrDefault(key, Boolean.class, false);
	}

	@Override
	public char getChar(String key) {
		return getOrDefault(key, Character.class, (char) 0);
	}

	@Override
	public DataContainer getStorage(String key) {
		return get(key, DataContainer.class);
	}

	@Override
	public Object put(String key, Object value) {
		String[] split = key.split("\\.", 2);
		if (split.length == 1) {
			if (value == null) {
				return base.remove(split[0]);
			} else {
				return base.put(split[0], value);
			}
		} else {
			DataContainer storage = getStorage(split[0]);
			if (storage == null) {
				storage = new DataMap();
				put(split[0], storage);
			}
			return storage.put(split[1], value);
		}
	}

	@Override
	public void putAll(Map<String, Object> values) {
		for (Map.Entry<String, Object> entry : values.entrySet()) {
			put(entry.getKey(), entry.getValue());
		}
	}

	@Override
	public Object remove(String key) {
		return put(key, null);
	}

	@Override
	public void clear() {
		base.clear();
	}

	@Override
	public Set<String> keySet() {
		return keySet(false);
	}

	@Override
	public Set<String> keySet(boolean deep) {
		Set<String> keys = new HashSet<>(base.size());
		for (String key : base.keySet()) {
			DataContainer storage = getStorage(key);
			if (storage == null) {
				keys.add(key);
			} else if (deep) {
				keys.addAll(storage.keySet(true));
			}
		}
		return keys;
	}

}

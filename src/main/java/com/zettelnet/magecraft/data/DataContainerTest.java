package com.zettelnet.magecraft.data;

public class DataContainerTest {

	public static void main(String[] args) {

		DataContainer storage = new DataMap();

		storage.put("test.key", "Hello World");

		for (String key : storage.keySet(true)) {
			System.out.println(key);
		}

		Object val = storage.get("test");

		System.out.println(val);
	}
}

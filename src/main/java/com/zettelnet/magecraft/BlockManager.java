package com.zettelnet.magecraft;

import java.util.Collection;

import org.bukkit.Location;
import org.bukkit.block.Block;

public interface BlockManager {

	Collection<CustomBlock> getRegisteredBlocks();

	Collection<CustomBlock> getRegisteredBlocks(CustomType type);

	CustomBlock getBlockByHandle(Block handle);

	CustomBlock getBlockByLocation(Location location);

	void registerBlocks(Collection<CustomBlock> blocks);

	boolean registerBlock(CustomBlock block);

	void unregisterAllBlocks();

	void unregisterBlocks(Collection<CustomBlock> blocks);

	boolean unregisterBlock(CustomBlock block);

	boolean isBlockRegistered(CustomBlock block);

	boolean isBlockHandleRegistered(Block handle);

	boolean isBlockLocationRegistered(Location location);

	@Deprecated
	CustomBlock loadBlock(CustomType type, Block handle);

	@Deprecated
	CustomBlock createBlock(CustomType type, Block handle);
}

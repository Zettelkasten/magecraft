package com.zettelnet.magecraft;

public interface Handleable<T> {

	T getHandle();
}

package com.zettelnet.magecraft;

import java.util.Collection;

import org.bukkit.Location;
import org.bukkit.block.Block;

public interface CustomBlock extends CustomObject, Handleable<Block> {

	Location getLocation();

	Collection<BlockManager> getRegisteredAt();

	boolean isRegisteredAt(BlockManager manager);

	boolean setRegisteredAt(BlockManager manager, boolean registered);
}

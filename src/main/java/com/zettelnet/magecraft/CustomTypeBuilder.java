package com.zettelnet.magecraft;

import java.util.Collection;

import org.bukkit.plugin.Plugin;

import com.zettelnet.magecraft.component.Component;
import com.zettelnet.magecraft.service.Service;

public interface CustomTypeBuilder {

	CustomTypeBuilder plugin(Plugin plugin);

	CustomTypeBuilder name(String name);

	CustomTypeBuilder service(Service service, Class<? extends Service> task) throws IllegalArgumentException;

	CustomTypeBuilder service(Service service, Collection<Class<? extends Service>> tasks) throws IllegalArgumentException;
	
	CustomTypeBuilder component(Component component);
	
	CustomTypeBuilder enabled(boolean enabled);
	
	boolean isReady();
	
	CustomType build() throws IllegalStateException;
}

package com.zettelnet.magecraft;

import java.util.Collection;

public interface TypeManager {

	CustomType getTypeByName(String typeName);

	Collection<CustomType> getRegisteredTypes();

	void registerTypes(Collection<CustomType> types);

	boolean registerType(CustomType type);

	void unregisterAllTypes();

	void unregisterTypes(Collection<CustomType> types);

	boolean unregisterType(CustomType type);

	boolean isTypeRegistered(CustomType type);

	boolean isTypeNameRegistered(String typeName);
}

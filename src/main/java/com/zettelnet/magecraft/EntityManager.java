package com.zettelnet.magecraft;

import java.util.Collection;

import org.bukkit.Location;
import org.bukkit.entity.Entity;

public interface EntityManager {

	Collection<CustomEntity> getRegisteredEntities();

	Collection<CustomEntity> getRegisteredEntities(CustomType type);

	CustomEntity getEntityByHandle(Entity handle);

	void registerEntities(Collection<CustomEntity> entities);

	boolean registerEntity(CustomEntity entity);

	void unregisterAllEntities();

	void unregisterEntities(Collection<CustomEntity> entities);

	boolean unregisterEntity(CustomEntity entity);

	boolean isEntityRegistered(CustomEntity entity);

	boolean isEntityHandleRegistered(Entity handle);

	@Deprecated
	CustomEntity createEntity(CustomType type, Location location);
}

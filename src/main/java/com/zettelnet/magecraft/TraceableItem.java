package com.zettelnet.magecraft;

import java.util.Collection;
import java.util.UUID;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public interface TraceableItem extends CustomItem {

	UUID getUniqueId();

	Collection<TraceableItemManager> getRegisteredAt();

	boolean isRegisteredAt(TraceableItemManager manager);

	boolean setRegisteredAt(TraceableItemManager manager, boolean registered);

	boolean updateHandle(ItemStack handle);

	boolean updateInventory(Inventory inventory);
}

package com.zettelnet.magecraft;

import java.util.Collection;

import org.bukkit.entity.Entity;

public interface CustomEntity extends CustomObject, Handleable<Entity> {

	Collection<EntityManager> getRegisteredAt();

	boolean isRegisteredAt(EntityManager manager);

	boolean setRegisteredAt(EntityManager manager, boolean registered);
}

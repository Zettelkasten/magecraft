package com.zettelnet.magecraft;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public interface CustomItem extends CustomObject, Handleable<ItemStack> {

	boolean isTraceable();

	Inventory getInventory();
}

package com.zettelnet.magecraft;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public interface ItemManager {

	CustomItem getItemByHandle(ItemStack handle, Inventory inventory);

	@Deprecated
	CustomItem createItem(CustomType type, int amount, Inventory inventory);
}

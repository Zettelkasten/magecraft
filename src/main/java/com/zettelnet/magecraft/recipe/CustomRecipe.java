package com.zettelnet.magecraft.recipe;

import org.bukkit.inventory.Recipe;

import com.zettelnet.magecraft.CustomType;

public interface CustomRecipe extends Recipe {

	CustomType getResultType();
}

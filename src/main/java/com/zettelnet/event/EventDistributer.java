package com.zettelnet.event;

import org.bukkit.event.Event;

/**
 * The distributer interface used to detect which custom objects are involved
 * and what their event targets are.
 * 
 * @author Zettelkasten
 * 
 * @param E
 *            the event class to distribute, usually {@link Event}
 *
 */
public interface EventDistributer<E> {

	/**
	 * Distributes an event by finding all custom objects involved and executing
	 * it by calling
	 * {@link DistributionExecutor#executeEvent(Event, com.zettelnet.magecraft.CustomObject, EventTarget)}
	 * for each custom object. The {@link EventTarget} is used to specify which
	 * role the object has if multiple custom objects are involved in one event.
	 * 
	 * @param executor
	 *            the executor to distribute the events to
	 * @param event
	 *            the event
	 * @see EventTarget
	 */
	void handleEvent(DistributionExecutor<E> executor, E event);
}

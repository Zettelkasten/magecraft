package com.zettelnet.event;

import org.bukkit.event.Event;

import com.zettelnet.magecraft.CustomObject;

/**
 * Represents a DistributionExecutor used by {@link EventDistributer}s to
 * execute the events.
 * 
 * @author Zettelkasten
 * 
 * @param E
 *            the event class to manage, usually {@link Event}
 *
 */
public interface DistributionExecutor<E> {

	/**
	 * Executes the event for all responsive listeners that are compatable with
	 * the event type, the involved {@link CustomObject} and the required
	 * target. This means, all {@link RegisteredComponentListener}s registered
	 * to the <tt>event.getClass()</tt> will be called using
	 * {@link RegisteredComponentListener#callEvent(Event, CustomObject, EventTarget)}
	 * 
	 * @param event
	 *            the event
	 * @param recipient
	 *            the recipient that the event is referred to or <tt>null</tt>
	 *            if there is no recipient (generic event)
	 * @param requiredTarget
	 *            the target
	 * @see RegisteredComponentListener#callEvent(Event, CustomObject,
	 *      EventTarget)
	 */
	void executeEvent(E event, Object recipient, EventTarget requiredTarget);
}

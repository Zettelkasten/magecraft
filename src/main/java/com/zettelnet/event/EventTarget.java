package com.zettelnet.event;

import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockFromToEvent;

import com.zettelnet.magecraft.event.target.ItemTarget;

/**
 * Interface to specify which custom objects should be listened to. This is
 * needed if multiple custom objects are involved in one event, for example
 * {@link BlockFromToEvent} has two blocks, <tt>getBlock()</tt> and
 * <tt>getToBlock()</tt>.
 * <p>
 * The EventTarget {@link #GENERIC} should be used if the event should be
 * listened to independent from the involved block.
 * 
 * @author Zettelkasten
 *
 */
public interface EventTarget {

	/**
	 * An EventTarget that marks the event to be called independent from the
	 * CustomObjects involved and therefore everytime it is called. This mimics
	 * the default Bukkit {@link EventHandler} annotation behaivior.
	 * 
	 * @see #isGeneric()
	 * @see GenericEventDistributer
	 */
	public static final EventTarget GENERIC = new EventTarget() {
		@Override
		public String name() {
			return "*";
		}

		@Override
		public boolean isGeneric() {
			return true;
		}

		@Override
		public boolean isAssignableFrom(EventTarget other) {
			return other == this;
		}
	};

	/**
	 * Returns the name of this event target. This is case-sensitive and usually
	 * uppecase.
	 * 
	 * @return the name
	 */
	String name();

	/**
	 * Returns whether the event is generic, meaning it should be called
	 * independent from the CustomObjects involved. This mimics the default
	 * Bukkit {@link EventHandler} annotation behaivior.
	 * 
	 * @return whether the event should be called independent from the
	 *         CustomObjects involved (everytime)
	 * @see #GENERIC
	 */
	boolean isGeneric();

	/**
	 * Checks whether this EventTarget is the same or a higher level EventTarget
	 * then another one.
	 * <p>
	 * For example, {@link ItemTarget#INVENTORY} is less specific then
	 * {@link ItemTarget#HOTBAR} and {@link ItemTarget#HAND} is even more
	 * specific (only these calls will return <tt>true</tt>):
	 * 
	 * <pre>
	 * ItemTarget.INVENTORY.isAssignableFrom(ItemTarget.INVENTORY)
	 * ItemTarget.INVENTORY.isAssignableFrom(ItemTarget.HOTBAR)
	 * ItemTarget.INVENTORY.isAssignableFrom(ItemTarget.HAND)
	 * 
	 * ItemTarget.HOTBAR.isAssignableFrom(ItemTarget.HOTBAR)
	 * ItemTarget.HOTBAR.isAssignableFrom(ItemTarget.HAND)
	 * 
	 * ItemTarget.HAND.isAssignableFrom(ItemTarget.HAND)
	 * </pre>
	 * 
	 * @param other
	 *            another EventTarget
	 * @return whether this EventTarget is the same or a higher level
	 *         EventTarget then another one
	 */
	boolean isAssignableFrom(EventTarget other);
}

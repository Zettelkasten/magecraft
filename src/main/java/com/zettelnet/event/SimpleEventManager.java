package com.zettelnet.event;

import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class SimpleEventManager<E> implements EventManager<E>, DistributionExecutor<E> {

	private final Map<EventPriority, Map<Class<? extends E>, Set<EventExecutor<E>>>> priorityExecutors;
	private final Map<EventReceiver, Set<EventExecutor<E>>> receiverExecutors;

	private final Set<EventDistributer<E>> distributers;

	public SimpleEventManager() {
		this.priorityExecutors = new EnumMap<>(EventPriority.class);
		for (EventPriority priority : EventPriority.values()) {
			priorityExecutors.put(priority, new HashMap<Class<? extends E>, Set<EventExecutor<E>>>());
		}
		this.receiverExecutors = new HashMap<>();

		this.distributers = new HashSet<>();
	}

	@Override
	public void callEvent(E event) {
		for (EventDistributer<E> distributer : distributers) {
			distributer.handleEvent(this, event);
		}
	}

	@Override
	public void executeEvent(E event, Object recipient, EventTarget requiredTarget) {
		for (Map<Class<? extends E>, Set<EventExecutor<E>>> executors : priorityExecutors.values()) {
			if (!executors.containsKey(event.getClass())) {
				continue;
			}
			for (EventExecutor<E> executor : executors.get(event.getClass())) {
				try {
					executor.execute(event, recipient, requiredTarget);
				} catch (EventException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void registerEvents(Collection<EventReceiver> receivers) {
		for (EventReceiver receiver : receivers) {
			registerEvents(receiver);
		}
	}

	@Override
	public void registerEvents(EventReceiver receiver) {
		// TODO Auto-generated method stub

	}

	@Override
	public void registerEvent(EventExecutor<E> executor) {
		Class<? extends E> event = executor.getEventType();

		// priorityExecutors
		Map<Class<? extends E>, Set<EventExecutor<E>>> executors = this.priorityExecutors.get(executor.getPriority());
		if (!executors.containsKey(event)) {
			executors.put(event, new HashSet<EventExecutor<E>>());
		}
		executors.get(event).add(executor);

		// componentExecutors
		if (!receiverExecutors.containsKey(executor.getReceiver())) {
			receiverExecutors.put(executor.getReceiver(), new HashSet<EventExecutor<E>>());
		}
		receiverExecutors.get(executor.getReceiver()).add(executor);
	}

	@Override
	public void unregisterEvents(Collection<EventReceiver> receivers) {
		for (EventReceiver receiver : receivers) {
			unregisterEvents(receiver);
		}
	}

	@Override
	public void unregisterEvents(EventReceiver receiver) {
		if (!receiverExecutors.containsKey(receiver)) {
			return;
		}
		for (Iterator<EventExecutor<E>> i = receiverExecutors.get(receiver).iterator(); i.hasNext();) {
			unregisterEvent(i.next());
			i.remove();
		}
	}

	@Override
	public void unregisterEvent(EventExecutor<E> executor) {

	}

	@Override
	public void unregisterAllEvents() {
		priorityExecutors.clear();
	}

	@Override
	public Set<EventDistributer<E>> getDistributers() {
		return distributers;
	}

	@Override
	public boolean addDistributer(EventDistributer<E> distributer) {
		return distributers.add(distributer);
	}

	@Override
	public boolean removeDistributer(EventDistributer<E> distributer) {
		return distributers.remove(distributer);
	}

}

package com.zettelnet.event;

public class EventException extends Exception {
	private static final long serialVersionUID = -5203432545978237236L;

	public EventException() {
		super();
	}

	public EventException(String message) {
		super(message);
	}

	public EventException(String message, Throwable cause) {
		super(message, cause);
	}

	public EventException(Throwable cause) {
		super(cause);
	}
}

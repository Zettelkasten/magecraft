package com.zettelnet.event;

import org.bukkit.event.Event;

import com.zettelnet.event.EventPriority;

/**
 * The listener interface for handling events.
 * 
 * @author Zettelkasten
 *
 * @param <E>
 *            the event class to manage, usually {@link Event}
 */
public interface EventExecutor<E> {

	/**
	 * Returns the {@link EventReceiver} this event executor is bound to.
	 * 
	 * @return the reciving class
	 */
	EventReceiver getReceiver();

	Class<? extends E> getEventType();

	EventPriority getPriority();

	EventTarget getTarget();

	/**
	 * Invoked when an event is called on a component with a specified
	 * CustomObject.
	 * 
	 * @param event
	 *            the event
	 * @param recipient
	 *            the recipient that the event is referred to or <tt>null</tt>
	 *            if there is no recipient (generic event)
	 * @param requiredTarget
	 *            the {@link EventTarget} that is required for this event to
	 *            pass
	 * @returns whether the execution has been successful
	 * @throws EventException
	 *             if an exception occurred during execution of the event
	 */
	boolean execute(E event, Object recipient, EventTarget requiredTarget) throws EventException;
}

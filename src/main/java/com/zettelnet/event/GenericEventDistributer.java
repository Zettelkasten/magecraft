package com.zettelnet.event;

import org.bukkit.event.Event;

import com.zettelnet.magecraft.CustomObject;

/**
 * An event distributer that will forward all events with the
 * {@link EventTarget#GENERIC} and no {@link CustomObject} (<tt>null</tt>).
 * 
 * @author Zettelkasten
 * 
 * @param E
 *            the event class to manage, usually {@link Event}
 *
 */
public class GenericEventDistributer<E> implements EventDistributer<E> {

	@Override
	public void handleEvent(DistributionExecutor<E> executor, E event) {
		executor.executeEvent(event, null, EventTarget.GENERIC);
	}
}

package com.zettelnet.event;

import java.util.Collection;
import java.util.Set;

import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.plugin.PluginManager;

import com.zettelnet.magecraft.CustomObject;
import com.zettelnet.magecraft.component.Component;
import com.zettelnet.magecraft.event.annotation.BlockHandler;
import com.zettelnet.magecraft.event.annotation.EntityHandler;
import com.zettelnet.magecraft.event.annotation.EventAnnotation;
import com.zettelnet.magecraft.event.annotation.ItemHandler;

/**
 * Event system that allows Components to listen to certain events only if they
 * are called on a {@link CustomObject} they are dedicated to. Additionally,
 * {@link EventTarget}s can be used to further specify which CustomObject is
 * meant.
 * <p>
 * This EventManager<Event> is using the Bukkit {@link PluginManager} and register
 * events there to forward them. It is compatable with the Bukkit event system.
 * <p>
 * {@link EventDistributer}s are used to find CustomObjects used in a event and
 * their EventTarget. Plugins adding new events can add EventDistributers to
 * distribute them appropriately.
 * 
 * @author Zettelkasten
 * 
 * @param <E>
 *            the event class to manage, usually {@link Event}
 */
public interface EventManager<E> {

	/**
	 * Calls an event to be handled by this event manager.
	 * 
	 * @param event
	 *            the event to be handled
	 */
	void callEvent(E event);

	/**
	 * Registers all events of the components.
	 * 
	 * @param components
	 *            collection containing Components to register the events from
	 * @see #registerEvents(Component)
	 */
	void registerEvents(Collection<EventReceiver> receivers);

	/**
	 * Registeres all methods in the Component that have an event annotation.
	 * The annotation can contain fields that specify further, for which events
	 * the method should be called. Calling this method multiple times will
	 * cause the events to be registered multiple times and therefore they will
	 * trigger multiple times. If no methods are event annotated, this method
	 * will have no effect.
	 * <p>
	 * An event annotated method has to have a class extending {@link Event} as
	 * first argument type and optionally when using an {@link CustomObject}
	 * handler the second argument type needs to be or extend CustomObject.
	 * 
	 * <pre>
	 * {@literal @}EventHandler
	 * public void onEvent(Event event[, CustomObject object]) {
	 *   // Execution code
	 * }
	 * </pre>
	 * 
	 * An method can contain multiple event annotations and will be registered
	 * once per annotation. This even works if the event annotations are
	 * different, but the second argument type then needs to be a superclass of
	 * both required types (for example when using
	 * <tt>{@literal @}ItemHandler</tt> and <tt>{@literal @}BlockHandler</tt>,
	 * the second argument type needs to be at least <tt>CustomObject</tt>).
	 * <p>
	 * The annotation {@link EventHandler} can be used to listen to all events
	 * with a certain event class on a priority. The default implementation
	 * supports {@link ItemHandler}, {@link BlockHandler} and
	 * {@link EntityHandler} which all use an {@link EventTarget} to specify on
	 * which object is specified.
	 * 
	 * @param component
	 *            the component to register all events from
	 */
	void registerEvents(EventReceiver receiver);

	/**
	 * Registeres all events of a certain type that satisfy the
	 * {@link EventTarget} specified in the {@link EventAnnotation} to a
	 * Component. When the event is triggered, the event executor will be
	 * called.
	 * 
	 * @param executor
	 *            the ComponentEventExecutor that will be called when the event
	 *            is executed
	 */
	void registerEvent(EventExecutor<E> executor);

	/**
	 * Unregisters all events of the components.
	 * 
	 * @param components
	 *            collection containing Components to unregister the events from
	 * @see #unregisterEvents(Component)
	 */
	void unregisterEvents(Collection<EventReceiver> receivers);

	/**
	 * Unregisters all events accociated with the Component.
	 * 
	 * @param component
	 *            the Component to unregister all events from
	 */
	void unregisterEvents(EventReceiver receiver);

	/**
	 * Unregisters a single {@link EventExecutor}.
	 * 
	 * @param executor
	 *            the event executor to unregister
	 */
	void unregisterEvent(EventExecutor<E> executor);

	/**
	 * Unregisters all events. After this operation, this EventManager<Event> no longer
	 * listens to any Bukkit events.
	 */
	void unregisterAllEvents();

	/**
	 * Returns all EventDistributers that are used to detect which custom
	 * objects are involved and what their event targets are. Any changes to
	 * this map will add or remove distributers to this EventManager.
	 * 
	 * @return a backed set containing all distributers
	 * @see EventDistributer
	 */
	Set<EventDistributer<E>> getDistributers();

	/**
	 * Adds the specified distributer.
	 * 
	 * @param distributer
	 *            the distributer to add
	 * @return <tt>true</tt> if the distributer was not already added
	 */
	boolean addDistributer(EventDistributer<E> distributer);

	/**
	 * Removes the specified distributer.
	 * 
	 * @param distributer
	 *            the distributer to remove
	 * @return <tt>true</tt> if the distributer had been added and now is
	 *         removed
	 */
	boolean removeDistributer(EventDistributer<E> distributer);
}
